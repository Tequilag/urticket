//
//  UINavigationBar+CustomHeight.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "UINavigationBar+CustomHeight.h"
#import "objc/runtime.h"

static char const *const heightKey = "Height";

@implementation UINavigationBar (CustomHeight)

- (void)setHeight:(CGFloat)height
{
    objc_setAssociatedObject(self, heightKey, @(height), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)height
{
    return objc_getAssociatedObject(self, heightKey);
}

- (CGSize)sizeThatFits:(CGSize)size
{
//    CGSize newSize;
//    
//    if (self.height) {
//        newSize = CGSizeMake(self.superview.bounds.size.width, [self.height floatValue]);
//    } else {
//        newSize = [super sizeThatFits:size];
//    }
//    
//    return newSize;
    CGSize newSize;
    
    if (self.height) {
        newSize = CGSizeMake(self.superview.bounds.size.width, [self.height floatValue]);
    } else {
        newSize = [super sizeThatFits:size];
        newSize.width = self.window.bounds.size.width;
    }
    
    return newSize;
}

@end

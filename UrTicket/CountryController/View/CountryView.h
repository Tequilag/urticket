//
//  CountryView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryView : UIView

@property (nonatomic, strong) UITableView *countryTable;
@property (nonatomic, strong) UISearchBar *searchBarCountry;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, assign) CGFloat topOffset;

@end

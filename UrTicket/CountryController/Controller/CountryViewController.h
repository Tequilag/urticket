//
//  CountryViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountryViewControllerDelegate <NSObject>

@end

typedef enum : NSUInteger {
    CountryViewControllerTypeCountry,
    CountryViewControllerTypeCity,
} CountryViewControllerType;

@interface CountryViewController : UIViewController

@property (nonatomic, assign) CountryViewControllerType typeOfController;
@property (nonatomic, strong) NSString *countryCode;

@property (nonatomic, weak) id<CountryViewControllerDelegate> delegate;

@end

//
//  UINavigationBar+CustomHeight.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (CustomHeight)

- (void)setHeight:(CGFloat)height;

@end

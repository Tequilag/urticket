//
//  FilterView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 28.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "FilterView.h"
#import "EFCircularSlider.h"
@implementation FilterView


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
   
    _transplantationView = [[UIView alloc] init];
    _transplantationView.backgroundColor = [UIColor greenColor];
    [self addSubview:_transplantationView];
    
    _departureView = [[UIView alloc] init];
    _departureView.backgroundColor = [UIColor whiteColor];
    _departureView.hidden = YES;
    [self addSubview:_departureView];
    
    _connectionDurationView = [[UIView alloc] init];
    _connectionDurationView.backgroundColor = [UIColor whiteColor];
    _connectionDurationView.hidden = YES;
    [self addSubview:_connectionDurationView];
    
    
    _toTransplantationView = [[UIButton alloc] init];
    _toTransplantationView.tag = 1;
    //_toTransplantationView.layer.backgroundColor = [UIColor redColor].CGColor;
    [_toTransplantationView setTitleColor: [UIColor colorWithRed:66/255.0
                                                           green:185/255.0
                                                            blue:244/255.0 alpha:1]
                                                        forState:UIControlStateNormal];
    [_toTransplantationView.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_toTransplantationView.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_toTransplantationView setTitle:@"Пересадки" forState:UIControlStateNormal];
    [self addSubview:_toTransplantationView];
    
    _toDepartureView = [[UIButton alloc] init];
    _toDepartureView.tag = 2;
    _toDepartureView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_toDepartureView setTitleColor: [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1] forState:UIControlStateNormal];
    [_toDepartureView.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_toDepartureView.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_toDepartureView setTitle:@"Время" forState:UIControlStateNormal];
    [self addSubview:_toDepartureView];
    
    _toConnectionDurationView = [[UIButton alloc] init];
    _toConnectionDurationView.tag = 3;
    _toConnectionDurationView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_toConnectionDurationView setTitleColor: [UIColor colorWithRed:66/255.0
                                                              green:185/255.0
                                                               blue:244/255.0 alpha:1]
                                                           forState:UIControlStateNormal];
    [_toConnectionDurationView.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_toConnectionDurationView.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_toConnectionDurationView setTitle:@"Длительность" forState:UIControlStateNormal];
    [self addSubview:_toConnectionDurationView];
    
    _connectionDurationSlider = [[UISlider alloc] init];
    _connectionDurationSlider.tag = 1;
    [self.connectionDurationView addSubview:_connectionDurationSlider];
    
    _connectionDurationSliderLabel = [[UILabel alloc] init];
    _connectionDurationSliderLabel.text = @"Любая";
    [self.connectionDurationView addSubview:_connectionDurationSliderLabel];
    
//    _departureSlider = [[UISlider alloc] init];
//    _departureSlider.tag = 2;
//    [self.departureView addSubview:_departureSlider];
//    
//    _departureSliderLabel = [[UILabel alloc] init];
//    _departureSliderLabel.text = @"Любое";
//    _departureSliderLabel.layer.backgroundColor = [UIColor redColor].CGColor;
//    [self.departureView addSubview:_departureSliderLabel];
//    
//    _arriveSlider = [[UISlider alloc] init];
//    _arriveSlider.tag = 3;
//    [self.departureView addSubview:_arriveSlider];
//    
//    _arriveSliderLabel = [[UILabel alloc] init];
//    _arriveSliderLabel.text = @"Любое";
//    _arriveSliderLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
//    [self.departureView addSubview:_arriveSliderLabel];
//    
    _labelFrom = [[UILabel alloc] init];
    //_labelFrom.text = @"Вылет";
    //_labelFrom.layer.backgroundColor = [UIColor greenColor].CGColor;
    [self.departureView addSubview:_labelFrom];
//
    _labelTo = [[UILabel alloc] init];
    //_labelTo.text = @"Прибытие";
    //_labelTo.layer.backgroundColor = [UIColor redColor].CGColor;
    [self.departureView addSubview:_labelTo];

    //CGRect minuteSliderFrame = CGRectMake(5, 170, 310, 310);
    _departureSlider = [[EFCircularSlider alloc] init];
    //_departureSlider.layer.backgroundColor = [UIColor greenColor].CGColor;
    _departureSlider.unfilledColor = [UIColor colorWithRed:23/255.0f green:47/255.0f blue:70/255.0f alpha:1.0f];
    _departureSlider.filledColor = [UIColor colorWithRed:155/255.0f green:211/255.0f blue:156/255.0f alpha:1.0f];
    //[_departureSlider setInnerMarkingLabels:@[@"5", @"10", @"15", @"20", @"25", @"30", @"35", @"40", @"45", @"50", @"55", @"60"]];
    [_departureSlider setInnerMarkingLabels:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12",
                                           @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"]];
    _departureSlider.labelFont = [UIFont systemFontOfSize:12.0f];
    _departureSlider.lineWidth = 8;
    _departureSlider.minimumValue = 0;
    _departureSlider.maximumValue = 24;
    _departureSlider.labelColor = [UIColor colorWithRed:76/255.0f green:111/255.0f blue:137/255.0f alpha:1.0f];
    _departureSlider.handleType = EFBigCircle;
    _departureSlider.handleColor = _departureSlider.filledColor;
    [self.departureView addSubview:_departureSlider];

    //CGRect hourSliderFrame = CGRectMake(55, 220, 210, 210);
    _arriveSlider = [[EFCircularSlider alloc] init];
    //_arriveSlider.layer.backgroundColor = [UIColor redColor].CGColor;
    _arriveSlider.unfilledColor = [UIColor colorWithRed:23/255.0f green:47/255.0f blue:70/255.0f alpha:1.0f];
    _arriveSlider.filledColor = [UIColor colorWithRed:98/255.0f green:243/255.0f blue:252/255.0f alpha:1.0f];
    //[_arriveSlider setInnerMarkingLabels:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12"]];
    [_arriveSlider setInnerMarkingLabels:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12",
                                           @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24"]];
    _arriveSlider.labelFont = [UIFont systemFontOfSize:12.0f];
    _arriveSlider.lineWidth = 8;
    _arriveSlider.snapToLabels = NO;
    _arriveSlider.minimumValue = 0;
    _arriveSlider.maximumValue = 24;
    _arriveSlider.labelColor = [UIColor colorWithRed:127/255.0f green:229/255.0f blue:255/255.0f alpha:1.0f];
    _arriveSlider.handleType = EFBigCircle;
    _arriveSlider.handleColor = _arriveSlider.filledColor;
    [self.departureView addSubview:_arriveSlider];

    _btn = [[UIButton alloc] init];
    _btn.layer.backgroundColor = [UIColor blueColor].CGColor;
    [self.departureView addSubview:_btn];
    
    _transpolationTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _transpolationTable.backgroundColor = [UIColor whiteColor];
    _transpolationTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _transpolationTable.allowsSelection = NO;
    _transpolationTable.scrollEnabled = NO;
    [self.transplantationView addSubview:_transpolationTable];
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
    CGFloat width = self.bounds.size.width;
    CGFloat offset = width * 0.05f;
    CGFloat widthButton = (width - offset*2.0f)/3.0f;
#pragma mark connectionDurationView
    self.toTransplantationView.frame = CGRectMake(offset,
                                                  offset,
                                                  widthButton,
                                                  20);
    self.toDepartureView.frame = CGRectMake(self.toTransplantationView.frame.origin.x + self.toTransplantationView.frame.size.width,
                                            offset,
                                            widthButton*0.9f,
                                            20);
    
    self.toConnectionDurationView.frame = CGRectMake(self.toDepartureView.frame.origin.x + self.toDepartureView.frame.size.width,
                                                     offset,
                                                     widthButton*1.1f,
                                                     20);
    
    self.transplantationView.frame = CGRectMake(0,
                                                self.toConnectionDurationView.frame.origin.y + self.toConnectionDurationView.frame.size.height +offset,
                                                width,
                                                self.bounds.size.height - self.transplantationView.frame.origin.y);

    self.departureView.frame = CGRectMake(0,
                                          self.toConnectionDurationView.frame.origin.y + self.toConnectionDurationView.frame.size.height +offset,
                                          width,
                                          self.bounds.size.height - self.departureView.frame.origin.y);
    
    self.connectionDurationView.frame = CGRectMake(0,
                                                   self.toConnectionDurationView.frame.origin.y + self.toConnectionDurationView.frame.size.height + offset,
                                                   width,
                                                    self.bounds.size.height - self.connectionDurationView.frame.origin.y);
    
    CGFloat height = self.transplantationView.frame.size.height*0.75f;
    self.connectionDurationSlider.frame = CGRectMake(offset,
                                   height/2,
                                   width - offset*2.0f,
                                   20);
    
    
    self.connectionDurationSliderLabel.frame = CGRectMake(width - offset*4.0f,
                                                          height/2 - 25,
                                                          offset*10.0f,
                                                          20);
#pragma mark departureView
    CGFloat sizeb = width*0.8f;
    self.departureSlider.frame = CGRectMake(width/2 - sizeb/2,
                                            offset,
                                            sizeb,
                                            sizeb);
//    self.btn.frame = CGRectMake(self.departureView.frame.origin.x + self.departureView.frame.size.width/2,// - (self.departureSlider.frame.size.width/2)*cos(45),
//                                        offset,// self.departureSlider.frame.origin.y + self.departureSlider.frame.size.height/2 - (self.departureSlider.frame.size.height/2)*sin(45),
//                                         20,
//                                         20);
        self.arriveSlider.frame = CGRectMake(self.departureSlider.frame.origin.x * 2.0f,
                                             self.departureSlider.frame.origin.y * 3.0f,
                                             self.departureSlider.frame.size.width*0.75f,
                                             self.departureSlider.frame.size.width*0.75f);
    
    self.labelFrom.frame = CGRectMake(offset,
                                      self.departureSlider.frame.origin.y + self.departureSlider.frame.size.height,
                                      width - offset*2.0f,
                                      20);
    
    
    
    self.labelTo.frame = CGRectMake(offset,
                                    self.labelFrom.frame.origin.y + self.labelFrom.frame.size.height + offset,
                                    width - offset*2.0f,
                                    20);
    
    self.transpolationTable.frame = CGRectMake(0,
                                               0,
                                               self.transplantationView.bounds.size.width,
                                               self.transplantationView.bounds.size.height);
    
    self.departureSlider.layer.cornerRadius = self.departureSlider.frame.size.width/2;
    self.arriveSlider.layer.cornerRadius = self.arriveSlider.frame.size.width/2;
    self.btn.layer.cornerRadius = self.btn.frame.size.width/2;
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}


@end

//
//  FilterView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 28.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFCircularSlider.h"

@interface FilterView : UIView

@property (nonatomic, assign) CGFloat topOffset;

@property (nonatomic, strong) UITableView *transpolationTable;

@property(nonatomic, strong) UIButton *toTransplantationView;
@property(nonatomic, strong) UIButton *toDepartureView;
@property(nonatomic, strong) UIButton *toConnectionDurationView;

@property(nonatomic, strong) UIView *transplantationView;
@property(nonatomic, strong) UIView *departureView;
@property(nonatomic, strong) UIView *connectionDurationView;

@property(nonatomic, strong) UISlider *connectionDurationSlider;
@property(nonatomic, strong) UILabel *connectionDurationSliderLabel;

@property(nonatomic, strong) EFCircularSlider *departureSlider;
@property(nonatomic, strong) UILabel *departureSliderLabel;

@property(nonatomic, strong) EFCircularSlider *arriveSlider;
@property(nonatomic, strong) UILabel *arriveSliderLabel;

@property(nonatomic, strong) UIButton *btn;

@property(nonatomic, strong) UILabel *timePeriodDepartureLabel;
@property(nonatomic, strong) UILabel *timePeriodArriveLabel;
@property(nonatomic, strong) UILabel *labelFrom;
@property(nonatomic, strong) UILabel *labelTo;

@end

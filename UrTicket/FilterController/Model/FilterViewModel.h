//
//  FilterViewModel.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 13.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface FilterViewModel : NSObject

@property (nonatomic, copy)NSArray *timeArray;
@property (nonatomic, copy)NSArray *countOfStopsArrayOld;
@property (nonatomic, retain)NSMutableArray *countOfStopsArrayNew;
@property (nonatomic) float durationTimeNew;
@property (nonatomic) float durationTimeOld;
@property (nonatomic) NSString* airportFrom;
@property (nonatomic) NSString* airportTo;
@end

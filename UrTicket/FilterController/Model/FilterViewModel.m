//
//  FilterViewModel.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 13.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "FilterViewModel.h"

@implementation FilterViewModel

@synthesize timeArray;
@synthesize countOfStopsArrayNew;
@synthesize countOfStopsArrayOld;
@synthesize durationTimeNew;
@synthesize durationTimeOld;
@synthesize airportTo;
@synthesize airportFrom;
@end

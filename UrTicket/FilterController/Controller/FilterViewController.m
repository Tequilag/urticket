//
//  FilterViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 29.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "FilterViewController.h"
#import "FilterView.h"
#import "EFCircularSlider.h"

@interface FilterViewController ()<UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) FilterView *filterView;
@property (nonatomic, strong) UIView *currentView;
@property (nonatomic, strong) UITapGestureRecognizer *gesture;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic , retain) NSMutableArray *parameters;

@end

CGFloat widthF;
CGFloat offsetF;

@implementation FilterViewController

-(void)loadView{
    [super loadView];
    self.filterView = [[FilterView alloc] init];
    self.view = self.filterView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.parameters = [[NSMutableArray alloc] init];
    [self.filterView.toTransplantationView addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.filterView.toDepartureView addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.filterView.toConnectionDurationView addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.filterView.connectionDurationSlider setContinuous:YES];
    self.filterView.connectionDurationSlider.minimumValue = 0;
    self.filterView.connectionDurationSlider.maximumValue = _filterViewModel.durationTimeOld;
    //self.filterView.connectionDurationSlider.value = _filterViewModel.durationTimeNew;
    [self.filterView.connectionDurationSlider setValue:_filterViewModel.durationTimeNew animated:YES];
    [self.filterView.connectionDurationSlider addTarget:self action:@selector(valueChangedSlider:) forControlEvents:UIControlEventValueChanged];
    
    self.filterView.departureSlider.currentValue = [[_filterViewModel.timeArray objectAtIndex:0] intValue];
    
    self.filterView.arriveSlider.currentValue = [[_filterViewModel.timeArray objectAtIndex:1] intValue];
    
    
    [self.filterView.departureSlider addTarget:self action:@selector(departureTimeChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.filterView.arriveSlider addTarget:self action:@selector(arriveTimeChanged:) forControlEvents:UIControlEventValueChanged];

    self.filterView.transpolationTable.delegate =self;
    self.filterView.transpolationTable.dataSource =self;
    self.currentView = self.filterView.transplantationView;
    [self departureTimeChanged:self.filterView.departureSlider];
    [self arriveTimeChanged:self.filterView.arriveSlider];
    //[self valueChangedSlider:self.filterView.connectionDurationSlider];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    widthF = self.view.bounds.size.width;
    offsetF = widthF * 0.05;
    [self valueChangedSlider:self.filterView.connectionDurationSlider];
    self.gesture = [[UITapGestureRecognizer alloc] init];
    self.gesture.numberOfTapsRequired = 1;
    self.gesture.numberOfTouchesRequired = 1;
    self.gesture.delegate = self;
    [self.gesture setCancelsTouchesInView:NO];
    [self.view.window addGestureRecognizer:self.gesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)departureTimeChanged:(EFCircularSlider*)slider {
    int newVal = (int)slider.currentValue >= 1  ? (int)slider.currentValue : 24;
    if (newVal != slider.maximumValue)
        self.filterView.labelFrom.text = [NSString stringWithFormat:@"Вылет: после %i ч.", newVal];
    else self.filterView.labelFrom.text = @"Вылет: в любое время";
}

-(void)arriveTimeChanged:(EFCircularSlider*)slider {
    int newVal = (int)slider.currentValue >= 1 ? (int)slider.currentValue : 24;
    if (newVal != slider.maximumValue)
        self.filterView.labelTo.text = [NSString stringWithFormat:@"Прибытие: до %i ч.", newVal];
    else self.filterView.labelTo.text = @"Прибытие: в любое время";
   }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.filterViewModel.countOfStopsArrayOld count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    UISwitch *switchCell = [[UISwitch alloc] initWithFrame:CGRectZero];
    NSInteger ind = [self.filterViewModel.countOfStopsArrayNew containsObject:[self.filterViewModel.countOfStopsArrayOld objectAtIndex:indexPath.row]];
    if (!ind) {
        switchCell.on = YES;
    }
    else {
        switchCell.on = NO;
    }
    [switchCell addTarget:self action:@selector(switchSelected:) forControlEvents:UIControlEventTouchUpInside];
    switchCell.tag = indexPath.row;
    cell.accessoryView = switchCell;
    if([[self.filterViewModel.countOfStopsArrayOld objectAtIndex:indexPath.row] intValue]== 0)
        cell.textLabel.text = @"Прямой рейс";
    else
        cell.textLabel.text = [NSString stringWithFormat:@"Пересадок: %@",[self.filterViewModel.countOfStopsArrayOld objectAtIndex:indexPath.row]];
    return cell;
}

-(void)switchSelected:(UISwitch *)sender{
    if(!sender.on){
        [self.filterViewModel.countOfStopsArrayNew addObject:[self.filterViewModel.countOfStopsArrayOld objectAtIndex:sender.tag]];
        sender.on = NO;
    }
    else{
        [self.filterViewModel.countOfStopsArrayNew removeObject:[self.filterViewModel.countOfStopsArrayOld objectAtIndex:sender.tag]];
        sender.on =YES;
    }
}

- (IBAction)valueChangedSlider:(UISlider *)slider {
    int value = slider.value;
    slider.value = value;
    CGRect trackRect = [slider trackRectForBounds:slider.bounds];
    CGRect thumbRect = [slider thumbRectForBounds:slider.bounds trackRect:trackRect value:slider.value];
    switch (slider.tag) {
        case 1:{
            CGFloat newX = thumbRect.origin.x + slider.frame.origin.x + self.filterView.connectionDurationSliderLabel.frame.size.width / 2.f;
            CGFloat newY = slider.frame.origin.y - thumbRect.size.height/2;
            if (value != slider.maximumValue)
                self.filterView.connectionDurationSliderLabel.text = [NSString stringWithFormat:@"%i", value];
            else
                {self.filterView.connectionDurationSliderLabel.text = @"Любая"; newX *= 0.96f;}
            [self.filterView.connectionDurationSliderLabel setCenter:CGPointMake(newX,newY)];
        }
            break;
        case 2:{
            CGFloat newX = thumbRect.origin.x + slider.frame.origin.x + self.filterView.departureSliderLabel.frame.size.width / 2.f;
             if (newX + self.filterView.departureSliderLabel.frame.size.width/2> widthF- offsetF)
                 newX = widthF - offsetF - self.filterView.departureSliderLabel.frame.size.width/2 ;
            CGFloat newY = slider.frame.origin.y - thumbRect.size.height/2;
            if (value != slider.minimumValue)
                self.filterView.departureSliderLabel.text = [NSString stringWithFormat:@"до %i", value];
            else
                {self.filterView.departureSliderLabel.text = @"Любое"; newX *= 0.96f;}
            [self.filterView.departureSliderLabel setCenter:CGPointMake(newX,newY)];
        }
            break;
        case 3:{
            CGFloat newX = thumbRect.origin.x + slider.frame.origin.x + self.filterView.arriveSliderLabel.frame.size.width / 2.f;
            if (newX + self.filterView.departureSliderLabel.frame.size.width/2> widthF- offsetF) newX = widthF - offsetF - self.filterView.departureSliderLabel.frame.size.width/2;
            CGFloat newY = slider.frame.origin.y - thumbRect.size.height/2;
            if (value != slider.maximumValue)
                self.filterView.arriveSliderLabel.text = [NSString stringWithFormat:@" после %i", value];
            else
                {self.filterView.arriveSliderLabel.text = @"Любое"; newX *= 0.96f;}
            [self.filterView.arriveSliderLabel setCenter:CGPointMake(newX,newY)];
        }
            break;
    }
   
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:nil];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication]
                             delegate] window];
    CGPoint pointInSubview = [self.view convertPoint:point
                                            fromView:mainWindow];
    if (!CGRectContainsPoint(self.view.frame, pointInSubview)) {
        [_filterViewModel setTimeArray:@[@(self.filterView.departureSlider.currentValue),
                                         @(self.filterView.arriveSlider.currentValue)]];
        [_filterViewModel setDurationTimeNew:(int)self.filterView.connectionDurationSlider.value];
        [self updateParametrs];
        [self.delegate filterBy:self.parameters filterViewModel:_filterViewModel];
        self.gesture.delegate = nil;
        [self.view.window removeGestureRecognizer:self.gesture];
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES;
}

-(void)changeView:(UIButton *)sender{
    self.currentView.hidden = YES;
    switch (sender.tag) {
        case 1:{
            self.currentView = self.filterView.transplantationView;
            self.currentView.hidden = NO;
        }
            break;
        case 2:{
            self.currentView = self.filterView.departureView;
            self.currentView.hidden = NO;
        }
            break;
        case 3:{
            self.currentView = self.filterView.connectionDurationView;
            self.currentView.hidden = NO;
        }
            break;
    }
}

-(void)updateParametrs{
    if ([self.filterViewModel.countOfStopsArrayNew count] != 0)
        [self.parameters addObject:@1];
    if (!([[self.filterView.labelTo.text componentsSeparatedByString:@":"][1] isEqualToString:@"в любое время"])||!([[self.filterView.labelFrom.text componentsSeparatedByString:@":"][1] isEqualToString:@"в любое время"]))
        [self.parameters addObject:@2];
    if (![self.filterView.connectionDurationSliderLabel.text isEqualToString:@"Любая"])
        [self.parameters addObject:@3];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

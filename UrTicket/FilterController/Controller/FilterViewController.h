//
//  FilterViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 29.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewModel.h"

@protocol FilterViewControllerDelegate;

@interface FilterViewController : UIViewController

@property (nonatomic, weak) id<FilterViewControllerDelegate> delegate;
@property (nonatomic, strong) FilterViewModel *filterViewModel;

@end

@protocol FilterViewControllerDelegate <NSObject>

-(void)filterBy:(NSArray *)parameter filterViewModel:(FilterViewModel *)model;

@end

//
//  Mapper.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreData/CoreData.h"

@interface Mapper : NSObject

+ (void)mapCityInContext:(NSManagedObjectContext *)context;
+ (void)mapCountryInContext:(NSManagedObjectContext *)context;
+ (void)mapAirportInContext:(NSManagedObjectContext *)context;

+ (NSMutableDictionary *)createJSONwithOptions;
+ (NSArray *)mapAirportsToArrayFromData:(NSData *)data;
+ (NSArray *)mapResponseJSONToArray:(NSData *)responseJSON;

@end

//
//  SearchViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewModel.h"
#import "TicketViewModel.h"


@protocol  SearchViewControllerDelegate;

@interface SearchViewController : UIViewController

@property (nonatomic, weak) id<SearchViewControllerDelegate> delegate;
@property (nonatomic, weak) NSString *selectedSortValue;
@property (nonatomic, strong) FilterViewModel *filterViewModel;
@property (nonatomic, strong) TicketViewModel *ticketViewModel;

@end

@protocol SearchViewControllerDelegate <NSObject>

- (void)reloadTables:(UIImage *)image;

@end

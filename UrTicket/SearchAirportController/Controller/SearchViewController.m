//
//  SearchViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchView.h"
#import "CalendarViewController.h"
#import "CountryViewController.h"
#import "AirportViewController.h"
#import "Mapper.h"
#import "QPXService.h"
#import "SearchTableViewCell.h"
#import "SearchViewCellContent.h"
#import "TicketView.h"
#import "TicketViewController.h"
#import "FilterViewController.h"
#import "DetailViewController.h"
#import "SortViewController.h"
#import "SearchViewModel.h"
#import "MainViewModel.h"

@interface SearchViewController () <UIPopoverPresentationControllerDelegate,
                                                        UITextFieldDelegate,
                                              AirportViewControllerDelegate,
                                                        UITableViewDelegate,
                                                      UITableViewDataSource,
                                                    FilterViewControllerDelegate,
                                                    TicketViewControllerDelegate>

@property (nonatomic, strong) SearchView *searchView;
@property (nonatomic, strong) TicketView *ticketView;
@property (nonatomic, strong) CalendarViewController *calendarViewController;
@property (nonatomic, strong) CountryViewController *countryViewController;
@property (nonatomic, strong) FilterViewController *filterViewController;
@property (nonatomic, strong) UITextField *selectedTextField;
@property (nonatomic, strong) NSMutableDictionary *mainOptions;
@property (nonatomic,strong) NSArray *resultArray;
@property (nonatomic,strong) NSMutableArray *filteredArray;
@property (nonatomic, strong) SearchViewModel *searchViewModel;
@property (nonatomic, strong) MainViewModel *mainViewModel;

@end

CGFloat widthS;
CGFloat heightS;
NSInteger currentTag;

@implementation SearchViewController

- (void)loadView {
    [super loadView];
    //_filterViewModel = [[FilterViewModel alloc] init];
    _searchViewModel = [[SearchViewModel alloc] init];
    [_filterViewModel setTimeArray:@[@0,@0]];
    self.searchView = [[SearchView alloc]init];
    self.view = self.searchView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Поиск авибилетов";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Magnifier"] style:UIBarButtonItemStyleDone target:self action:@selector(getResult:)];
    UIBarButtonItem *btn_Back = [[UIBarButtonItem alloc] initWithTitle:@"Назад"
                                                                 style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem=btn_Back;
    
    self.mainOptions = [NSMutableDictionary dictionaryWithDictionary:[Mapper createJSONwithOptions]];
    
    self.mainViewModel = [[MainViewModel alloc] init];
    
    if (!self.ticketViewModel){
        self.ticketViewModel = [[TicketViewModel alloc] init];
        self.ticketViewModel.countOfAdults = @"1";
        self.ticketViewModel.countOfTeens = @"0";
        self.ticketViewModel.countOfChildren = @"0";
        self.ticketViewModel.prefferedCabin = @"Эконом";
        self.ticketViewModel.airline = @"Oneworld";
    }
    else
    {
        self.selectedTextField = self.searchView.cityFrom;
        [self setAirport:[self.mainViewModel getCityNameByCityCode:[self.mainViewModel
                                             getCityCodeByAirportCode:self.ticketViewModel.from ]] andCode:self.ticketViewModel.from];
        self.selectedTextField = self.searchView.cityTo;
        [self setAirport:[self.mainViewModel getCityNameByCityCode:[self.mainViewModel
                                                                    getCityCodeByAirportCode:self.ticketViewModel.to ]] andCode:self.ticketViewModel.to];
        self.selectedSortValue = @"Цена";
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"date" object:[dateFormat stringFromDate:self.ticketViewModel]];
    }
   
    self.searchView.dateArrive.delegate = self;
    self.searchView.dateDeparture.delegate = self;
    
    self.searchView.cityTo.delegate = self;
    self.searchView.cityFrom.delegate = self;
    
    self.searchView.resultTable.delegate = self;
    self.searchView.resultTable.dataSource = self;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateDidChanged:) name:@"date" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dateDidChanged:) name:@"ticket" object:nil];
    [self.searchView.filter addTarget:self action:@selector(openController:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView.swapCities addTarget:self action:@selector(swap) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView.countOfTickets addTarget:self action:@selector(openController:) forControlEvents:UIControlEventTouchUpInside];
    
    _filterViewModel.countOfStopsArrayNew = [[NSMutableArray alloc] init];
}
- (void)navigationBar:(UINavigationBar *)navigationBar didPopItem:(UINavigationItem *)item{
    
}

-(void) Back{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.delegate reloadTables:([self captureScreen])];
}

- (UIImage *) captureScreen {
//    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
//    CGRect rect = CGRectMake(keyWindow.frame.origin.x,
//                             keyWindow.frame.origin.y,
//                             keyWindow.frame.size.width,
//                             keyWindow.frame.size.height/2);
    CGRect rect = self.searchView.screenShot.bounds;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.searchView.scroll.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //self.sa.imageView.image = img;
    return img;
}

-(void) viewDidAppear:(BOOL)animated{
    widthS = self.view.bounds.size.width;
    heightS = self.view.bounds.size.height;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dateDidChanged:(NSNotification *)notification {
    NSString *str = (NSString *)notification.object;
    NSDateFormatter *dateFormatTo = [[NSDateFormatter alloc] init];
    [dateFormatTo setDateFormat:@"dd.MM.yy"];
    NSDateFormatter *dateFormatFrom = [[NSDateFormatter alloc] init];
    [dateFormatFrom setDateFormat:@"yyyy-MM-dd"];
    [self.ticketViewModel setDateOfDeparture:[dateFormatFrom dateFromString:str]];
    [self.mainOptions setValue:str forKeyPath:@"request.slice.date"];
    self.selectedTextField.text =[dateFormatTo stringFromDate:[dateFormatFrom dateFromString:str]];
  }
-(void)swap{
    NSString *temp =self.ticketViewModel.from;;
    self.ticketViewModel.from = self.ticketViewModel.to;
    self.ticketViewModel.to = temp;
    [self.mainOptions setValue:self.ticketViewModel.to forKeyPath:@"request.slice.destination"];
    [self.mainOptions setValue:self.ticketViewModel.from forKeyPath:@"request.slice.origin"];
    temp = self.searchView.cityFrom.text;
    self.searchView.cityFrom.text = self.searchView.cityTo.text;
    self.searchView.cityTo.text = temp;
}
- (void)ticketSelected:(NSNotification *)notification {
    NSString *str = (NSString *)notification.object;
    [self.mainOptions setValue:str forKeyPath:@"request.slice.date"];
    self.selectedTextField.text =str;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    NSLog(@"Start scroll");
//    
//    //I assume the buttons are within your cells so you will have to enable them within your cells so you will probably have to handle this by sending a custom message to your cells or accessing them with properties.
//    CGRect frame = self.searchView.countOfTickets.frame;
//    frame.origin.y = self.searchView.scroll.contentOffset.y + self.searchView.resultTable.frame.size.height  + 4;
//    self.searchView.countOfTickets.frame = frame;
//    
//    [self.view bringSubviewToFront:self.searchView.countOfTickets];}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    NSLog(@"End scroll");
//    
//    // do the same to enable them back
//    CGRect frame = self.searchView.countOfTickets.frame;
//    frame.origin.y = self.searchView.scroll.contentOffset.y + self.searchView.resultTable.frame.size.height  + 4;
//    self.searchView.countOfTickets.frame = frame;
//    
//    [self.view bringSubviewToFront:self.searchView.countOfTickets];}

- (void)openController:(UIButton *)sender {
    switch (sender.tag) {
        case 1:{
            TicketViewController *ticketViewController = [[TicketViewController alloc ] init];
            if (self.ticketViewModel)
            ticketViewController.ticketViewModel = self.ticketViewModel;
            ticketViewController.delegate =self;
            ticketViewController.preferredContentSize = CGSizeMake(widthS,heightS*0.75f);
            ticketViewController.modalPresentationStyle = UIModalPresentationPopover;
            ticketViewController.popoverPresentationController.sourceView = sender;
            ticketViewController.popoverPresentationController.sourceRect = sender.bounds;
            ticketViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            ticketViewController.popoverPresentationController.delegate = self;
            [self presentViewController:ticketViewController animated:YES completion:nil];
        }
            break;
        case 2:{
            SortViewController *sortViewController = [[SortViewController alloc ] init];
            sortViewController.sortValue = self.selectedSortValue;
            sortViewController.preferredContentSize = CGSizeMake(widthS,heightS*0.75f);
            sortViewController.modalPresentationStyle = UIModalPresentationPopover;
            sortViewController.popoverPresentationController.sourceView = sender;
            sortViewController.popoverPresentationController.sourceRect = sender.bounds;
            sortViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            sortViewController.popoverPresentationController.delegate = self;
            [self presentViewController:sortViewController animated:YES completion:nil];
        }
            break;
            
        case 3:{
            FilterViewController *filterViewController = [[FilterViewController alloc ] init];
            if (self.filterViewModel)
            filterViewController.filterViewModel = self.filterViewModel;
            filterViewController.delegate = self;
            filterViewController.preferredContentSize = CGSizeMake(widthS,heightS*0.8f);
            filterViewController.modalPresentationStyle = UIModalPresentationPopover;
            filterViewController.popoverPresentationController.sourceView = sender;
            filterViewController.popoverPresentationController.sourceRect = sender.bounds;
            filterViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
            filterViewController.popoverPresentationController.delegate = self;
            [self presentViewController:filterViewController animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.filteredArray)
    self.navigationItem.title = [NSString stringWithFormat:@"%lu из %lu", (unsigned long)[self.filteredArray count], (unsigned long)[self.resultArray count]];
    NSInteger tempint =[self.filteredArray count];
    return [self.filteredArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchTableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:[SearchTableViewCell reuseIdentifier]];
    //if (!cell) {
       cell = [[SearchTableViewCell alloc] init];
    //}
    if ([[[self.filteredArray objectAtIndex:indexPath.row] valueForKey:@"added"] isEqual:@"NO"])
    [cell.cellContentView.addToLiked addTarget:self action:@selector(addToLiked:) forControlEvents:UIControlEventTouchUpInside];
    else {
        [cell.cellContentView.addToLiked setImage:[UIImage imageNamed:@"addedStarBlack"] forState:UIControlStateNormal];
            [cell.cellContentView.addToLiked addTarget:self action:@selector(deleteFavorite) forControlEvents:UIControlEventTouchUpInside];
    }
    cell.cellContentView.addToLiked.tag = indexPath.row;
    [cell setItem:[self.filteredArray objectAtIndex:indexPath.row] originName:self.searchView.cityFrom.text
                                                            destinationName:self.searchView.cityTo.text];
    return cell;
}

- (void)addToLiked:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    [sender setImage:[UIImage imageNamed:@"addedStarBlack"] forState:UIControlStateNormal];
    [[self.resultArray objectAtIndex:[self.resultArray indexOfObject:[self.filteredArray objectAtIndex:sender.tag]]] setValue:@"YES" forKey:@"added"];
    [[self.filteredArray objectAtIndex:sender.tag] setValue:@"YES" forKey:@"added"];
    [sender addTarget:self action:@selector(deleteFavorite) forControlEvents:UIControlEventTouchUpInside];
    [self.searchViewModel addLikedToDataBase:[self.filteredArray objectAtIndex:sender.tag] withComplition:^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Добавлено в избранное!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ОК"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        
        [alert addAction:yesButton];
        [weakSelf presentViewController:alert animated:YES completion:nil];
    }];
}
-(void) deleteFavorite{
    
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchTableViewCell *cell = (SearchTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    DetailViewController *detailViewController = [[DetailViewController alloc]init];
    [self.searchViewModel addRecentlyViewed:[self.filteredArray objectAtIndex:cell.cellContentView.addToLiked.tag]];//indexPath.row]];
    detailViewController.resultItem = [self.filteredArray objectAtIndex:cell.cellContentView.addToLiked.tag];//indexPath.row];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (CGRectGetHeight(tableView.frame)*0.52);//190.0f;
}

- (void)getResult:(UIButton *)sender {
    if ((self.ticketViewModel.from != nil)&&(self.ticketViewModel.to != nil)&&(self.ticketViewModel.dateOfDeparture != nil)){
        [self.searchView.indicatorView startAnimating];
    self.ticketViewModel.dateOfRequest = [NSDate date];
    [self.searchViewModel addRepeatedSearch:self.ticketViewModel];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [[QPXService sharedService] getResult:self.mainOptions withComplition:^(NSArray *result){
        dispatch_async(dispatch_get_main_queue(),^{
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            if (!([result count] == 0)) {
                self.resultArray = result;
                self.filteredArray =[self.resultArray mutableCopy];
                [self.searchView.resultTable reloadData];
                [self.searchView.indicatorView stopAnimating];
                [self withComplition:^(NSArray *stops, int maxDuration){
                    self.filterViewModel.durationTimeOld = maxDuration/60 + 1;
                    self.filterViewModel.durationTimeNew = self.filterViewModel.durationTimeOld;
                    [self.filterViewModel setCountOfStopsArrayOld:stops];
                }];
               [self.searchView.filter setTitleColor:[UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1] forState:UIControlStateNormal] ;
                self.searchView.filter.enabled = YES;
                
            }
            else {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"По заданным параметрам рейсов не найдено!"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"ОК"
                                            style:UIAlertActionStyleDefault
                                            handler:nil];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
    }];
    }
    else
    {
        __weak typeof(self) weakSelf = self;
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"Введите место вылета и назначения с датой вылета."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"ОК"
                                        style:UIAlertActionStyleDefault
                                        handler:nil];
            
            [alert addAction:yesButton];
            [weakSelf presentViewController:alert animated:YES completion:nil];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.selectedTextField = textField;
    if (textField.tag >2){
        self.calendarViewController = [[CalendarViewController alloc ] init];
        self.calendarViewController.preferredContentSize = CGSizeMake(widthS,heightS*0.75f);
        self.calendarViewController.modalPresentationStyle = UIModalPresentationPopover;
        self.calendarViewController.popoverPresentationController.sourceView = textField;
        self.calendarViewController.popoverPresentationController.sourceRect = textField.bounds;
        self.calendarViewController.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        self.calendarViewController.popoverPresentationController.delegate = self;
        [self presentViewController:self.calendarViewController animated:YES completion:nil];
    }
    else {
        self.countryViewController = [[CountryViewController alloc] init];
        [self.navigationController pushViewController:self.countryViewController animated:YES];
        [self.view endEditing:YES];
    }
    return NO;
}

-(void) dismissMe{
     [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setAirport:(NSString *)airport andCode:(NSString *)code{
    if (self.selectedTextField.tag==1){
        [self.selectedTextField setText:[NSString stringWithFormat:@"%@",airport]];
        self.ticketViewModel.from = code;
        [self.mainOptions setValue:code forKeyPath:@"request.slice.origin"];
    }
    else {
        [self.selectedTextField setText:[NSString stringWithFormat:@"%@",airport]];
        self.ticketViewModel.to = code;
        [self.mainOptions setValue:code forKeyPath:@"request.slice.destination"];
    }
}

-(void)filterBy:(NSArray *)parameter filterViewModel:(FilterViewModel *)model{
    self.filterViewModel = model;
    NSArray *time = self.filterViewModel.timeArray;
    NSArray *countArray = self.filterViewModel.countOfStopsArrayNew;
    size_t sizeCount = [countArray count];
    self.filteredArray = [self.resultArray mutableCopy];
    int v1 = [[time objectAtIndex:0] intValue];
    int v2 = [[time objectAtIndex:1] intValue];
    if (v2 == 0) v2 = 1000;
    for (int i =0;i < [parameter count];++i){
    switch ([[parameter objectAtIndex:i] intValue]) {
        case 1:{//Переcадки
            for (NSInteger i = [self.filteredArray count] - 1; i >= 0; --i ){
                NSArray *segment = [self.filteredArray[i] objectForKey:@"segment"];
                v1 = (int)[segment count]/2;
                for (NSInteger j = sizeCount-1; j >= 0; --j){
                    if (v1 == [[countArray  objectAtIndex:j] intValue])
                        [self.filteredArray removeObjectAtIndex:i];
                }
            }
        }
            break;
        case 2:{//время
            for (NSInteger i = [self.filteredArray count] - 1; i >= 0; --i ){
                NSArray *segment = [self.filteredArray[i] objectForKey:@"segment"];
                if (( [[NSString stringWithFormat:@"%@",[[[[segment firstObject] objectForKey:@"departureTime"] componentsSeparatedByString:@"T"][1] componentsSeparatedByString:@":"][0]] intValue] < v1) || ([[NSString stringWithFormat:@"%@",[[[[segment firstObject] objectForKey:@"arrivalTime"] componentsSeparatedByString:@"T"][1] componentsSeparatedByString:@":"][0]] intValue] > v2))
                    [self.filteredArray removeObjectAtIndex:i];
            }

        }
            break;
        case 3:{//Длительность полета
            int duration = model.durationTimeNew * 60;
            for (NSInteger i = [self.filteredArray count] - 1; i >= 0; --i ){
                v1 = 0;
                NSArray *segment = [self.filteredArray[i] objectForKey:@"segment"];
                size_t sizes = [segment count];
                for (size_t j = 0; j < sizes; j++){
                    v1+=[[segment[j] objectForKey:@"duration"] intValue];
                    v1+=[[segment[j] objectForKey:@"connectionDuration"] intValue];
                }
                if (v1 > duration)
                    [self.filteredArray removeObjectAtIndex:i];
            }
        }
            break;
        default:
            break;
    }
    }
    [self.searchView.resultTable reloadData];
}

-(void)addobjectToFilteredArray:(int)value forCheck:(int)check{
    
}

- (void)withComplition:(void (^)(NSArray  *StopsArray, int maxDuration))completion {
    size_t sizei = [self.resultArray count];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    int stops;
    int duration = 0;
    int tempDuration;
    for (size_t i = 0; i < sizei; ++i){
        tempDuration = 0;
        NSArray *segment = [self.filteredArray[i] objectForKey:@"segment"];
        size_t sizej = [segment count];
        stops = (int)sizej/2;
        if (![tempArray containsObject:@(stops)])
            [tempArray addObject:@(stops)];
        for (size_t j=0; j < sizej; ++j){
            tempDuration+=[[segment[j] objectForKey:@"duration"] intValue];
            tempDuration+=[[segment[j] objectForKey:@"connectionDuration"] intValue];
        }
        if(tempDuration > duration)
            duration = tempDuration;
    }
    [tempArray sortUsingSelector:@selector(compare:)];
    completion(tempArray, duration);
}

-(void)setTicket:(TicketViewModel *)value{
    self.ticketViewModel = value;
    NSDictionary *preferredCabinValue = @{
                                 @"Эконом" :        @"COACH",
                                 @"Премиум" :       @"PREMIUM_COACH",
                                 @"Бизнес" :        @"BUSINESS",
                                 @"Первый" :        @"FIRST",
                                 };
    NSDictionary *alliance = @{
                                @"Oneworld" :       @"ONEWORLD",
                                @"SkyTeam" :        @"SKYTEAM",
                                @"Star" :           @"STAR"
                                          };
    [self.mainOptions setValue:[NSString stringWithFormat:@"%@", value.countOfAdults] forKeyPath:@"request.passengers.adultCount"];
    [self.mainOptions setValue:[NSString stringWithFormat:@"%@", value.countOfTeens] forKeyPath:@"request.passengers.childCount"];
    [self.mainOptions setValue:[NSString stringWithFormat:@"%@", value.countOfChildren] forKeyPath:@"request.passengers.infantCount"];
    [self.mainOptions setValue:[preferredCabinValue valueForKey:value.prefferedCabin] forKeyPath:@"request.slice.preferredCabin"];
    [self.mainOptions setValue:[alliance valueForKey:value.airline ] forKeyPath:@"request.slice.alliance"];
}

//- (UIImage *) captureScreen {
//    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
//    CGRect rect = [keyWindow bounds];
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [keyWindow.layer renderInContext:context];
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return img;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a 	little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//            for (size_t idx_i = 0; idx_i < size - 1; idx_i++)
//                for (size_t idx_j = 0; idx_j < size - idx_i - 1; idx_j++){
//                    v1 = [self.resultArray[idx_j + 1] objectForKey:@"ti"]
//                    if (self.resultArray[idx_j + 1] < self.resultArray[idx_j])
//                        [self.resultArray exchangeObjectAtIndex:idx_j + 1 withObjectAtIndex:idx_j];
//                }


@end

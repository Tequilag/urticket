//
//  SearchViewModel.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 18.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TicketViewModel.h"

@interface SearchViewModel : NSObject

- (void)addLikedToDataBase:(id)object withComplition:(void (^)())completion;
- (void)addRepeatedSearch:(TicketViewModel *)object;
- (void)addRecentlyViewed:(id)item;

@end

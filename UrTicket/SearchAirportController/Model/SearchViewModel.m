//
//  SearchViewModel.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 18.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SearchViewModel.h"
#import "CoreData/CoreData.h"
#import "AppDelegate.h"
#import "Favorite+CoreDataClass.h"
//#import "PairAirports+CoreDataClass.h"
#import "RecentlyViewed+CoreDataClass.h"
#import "RepeatedSearch+CoreDataClass.h"

@interface SearchViewModel()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation SearchViewModel


- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)addLikedToDataBase:(id)object withComplition:(void (^)())completion {
    
    Favorite *favorite = [NSEntityDescription insertNewObjectForEntityForName:@"Favorite" inManagedObjectContext:self.persistentContainer.viewContext];
    NSMutableOrderedSet *pairAirports = [[NSMutableOrderedSet alloc] init];
//    NSArray *segment = [object objectForKey:@"segment"];
//    for (id curr in segment) {
//        PairAirports *pair = [NSEntityDescription insertNewObjectForEntityForName:@"PairAirports" inManagedObjectContext:self.persistentContainer.viewContext];
//        pair.airportFrom = [curr objectForKey:@"origin"];
//        pair.airportTo = [curr objectForKey:@"destination"];
//        pair.timeArrival = [curr objectForKey:@"arrivalTime"];
//        pair.timeDeparture = [curr objectForKey:@"departureTime"];
//        [pairAirports addObject:pair];
//    }
    favorite.saleTotal = [object objectForKey:@"saleTotal"];
    
    favorite.pairAirports = pairAirports;
    favorite.savedArray = [NSKeyedArchiver archivedDataWithRootObject:object];
    favorite.dateOfSave = [NSDate date];
    NSError *error = nil;
    [self.persistentContainer.viewContext save:&error];
    if (completion){
        completion();
    }
}

- (void)addRecentlyViewed:(id)item{
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"RecentlyViewed" inManagedObjectContext:self.persistentContainer.viewContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];

    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateOfSave" ascending:YES];
    [request setSortDescriptors:@[sort]];
    
    NSError *error = nil;
    NSArray *objects = [self.persistentContainer.viewContext executeFetchRequest:request
                                              error:&error];
    if ([objects count] >=5){
        RecentlyViewed *rViewed = [objects firstObject];
        [self.persistentContainer.viewContext deleteObject:rViewed];
        
    }
    RecentlyViewed *newEntity = [NSEntityDescription insertNewObjectForEntityForName:@"RecentlyViewed"
                                                     inManagedObjectContext:self.persistentContainer.viewContext];
    newEntity.savedArray = [NSKeyedArchiver archivedDataWithRootObject:item];
    newEntity.dateOfSave = [NSDate date];
    [self.persistentContainer.viewContext save:nil];
}

- (void)addRepeatedSearch:(TicketViewModel *)object{
    
    NSError *error = nil;
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"RepeatedSearch" inManagedObjectContext:self.persistentContainer.viewContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateOfSave" ascending:YES];
    [request setSortDescriptors:@[sort]];
    
    NSArray *objects = [self.persistentContainer.viewContext executeFetchRequest:request
                                                                           error:&error];
    if ([objects count] >=5){
        RepeatedSearch *rViewed = [objects firstObject];
        [self.persistentContainer.viewContext deleteObject:rViewed];
        
    }
    
    RepeatedSearch *rSearch = [NSEntityDescription insertNewObjectForEntityForName:@"RepeatedSearch"
                                                   inManagedObjectContext:self.persistentContainer.viewContext];
    NSMutableDictionary *forSave = [[NSMutableDictionary alloc] init];
    [forSave setObject:object.countOfAdults forKey:@"countOfAdults"];
    [forSave setObject:[object valueForKey:@"countOfTeens"] forKey:@"countOfTeens"];
    [forSave setObject:[object valueForKey:@"countOfChildren"] forKey:@"countOfChildren"];
    [forSave setObject:[object valueForKey:@"prefferedCabin"] forKey:@"prefferedCabin"];
    [forSave setObject:[object valueForKey:@"airline"] forKey:@"airline"];
    [forSave setObject:[object valueForKey:@"from"] forKey:@"from"];
    [forSave setObject:[object valueForKey:@"to"] forKey:@"to"];
    [forSave setObject:[object valueForKey:@"dateOfRequest"] forKey:@"dateOfRequest"];
    [forSave setObject:[object valueForKey:@"dateOfDeparture"] forKey:@"dateOfDeparture"];
    rSearch.savedArray = [NSKeyedArchiver archivedDataWithRootObject:forSave];
    rSearch.dateOfSave = [NSDate date];
    //rSearch.savedFields = [NSKeyedArchiver archivedDataWithRootObject:object2];
    

    [self.persistentContainer.viewContext save:&error];
}
@end

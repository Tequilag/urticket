//
//  SearchView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    
    _scroll = [[UIScrollView alloc] init];
    [_scroll setScrollEnabled:YES];
    [self addSubview:_scroll];
    
    
    UIColor *buttonColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
    _resultTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _resultTable.backgroundColor = [UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0];
    _resultTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.scroll addSubview:_resultTable];
    
    _cityFrom = [[UITextField alloc] init];
    _cityFrom.tag = 1;
    _cityFrom.placeholder = @"Выберите город";
    [_cityFrom setFont:[UIFont boldSystemFontOfSize:13]];
    _cityFrom.textAlignment = NSTextAlignmentLeft;
    _cityFrom.borderStyle = UITextBorderStyleRoundedRect;
    [self.scroll addSubview:_cityFrom];
    
    _cityTo = [[UITextField alloc] init];
    _cityTo.tag = 2;
    _cityTo.placeholder = @"Выберите город";
    [_cityTo setFont:[UIFont boldSystemFontOfSize:13]];
    _cityTo.textAlignment = NSTextAlignmentLeft;
    _cityTo.borderStyle = UITextBorderStyleRoundedRect;
    [self.scroll addSubview:_cityTo];
    
    
    _dateDeparture = [[UITextField alloc] init];
    _dateDeparture.tag = 3;
    _dateDeparture.placeholder = @"Выберите дату";
    [_dateDeparture setFont:[UIFont boldSystemFontOfSize:13]];
    _dateDeparture.textAlignment = NSTextAlignmentLeft;
    _dateDeparture.borderStyle = UITextBorderStyleRoundedRect;
    [self.scroll addSubview:_dateDeparture];
    
    _countOfTickets = [[UIButton alloc] init];
    _countOfTickets.tag = 1;
    _countOfTickets.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_countOfTickets setAlpha:1];
    [_countOfTickets setTitleColor:buttonColor forState:UIControlStateNormal];
    [_countOfTickets.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_countOfTickets.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_countOfTickets setTitle:@"Билет" forState:UIControlStateNormal];
    [self.scroll addSubview:_countOfTickets];

    _swapCities = [[UIButton alloc] init];
    _swapCities.tag = 1;
    _swapCities.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _swapCities.layer.borderColor = [UIColor blackColor].CGColor;
    _swapCities.layer.borderWidth = 1.0f;
    [_swapCities setAlpha:1];
    [_swapCities setImage:[UIImage imageNamed:@"swap"] forState:UIControlStateNormal];
    [self.scroll addSubview:_swapCities];
    
    _filter = [[UIButton alloc] init];
    _filter.tag = 3;
    _filter.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_filter setTitleColor:[UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0] forState:UIControlStateNormal];
    [_filter.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_filter.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_filter setTitle:@"Фильтры" forState:UIControlStateNormal];
    _filter.enabled = NO;
    
    [self.scroll addSubview:_filter];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.scroll addSubview:_indicatorView];
    

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width  = self.bounds.size.width;
    CGFloat height  = self.bounds.size.height;
    CGFloat offset = width * 0.1f;
    CGFloat textFieldHeight = height*0.05f;
    CGFloat textFieldWidth = width*0.35f;
    CGFloat buttonWidth = (width - width*0.01f*2)/2 - width*0.01f;
    
    self.scroll.frame = CGRectMake(0,
                                   0,
                                   width,
                                   height);
    
    self.indicatorView.center = self.center;
    
    self.cityFrom.frame = CGRectMake(offset,
                                     offset,
                                     textFieldWidth,
                                     textFieldHeight);
    
    self.swapCities.frame = CGRectMake(self.cityFrom.frame.origin.x + self.cityFrom.frame.size.width,// + width*0.01f,
                                       self.cityFrom.frame.origin.y,
                                       width * 0.1f,
                                       width * 0.1f);
    
    self.swapCities.layer.cornerRadius = self.swapCities.frame.size.width/2;
    
    self.cityTo.frame = CGRectMake(self.swapCities.frame.origin.x + self.swapCities.frame.size.width,//self.cityFrom.frame.origin.x + textFieldWidth + width*0.01f,
                                     offset,
                                     textFieldWidth,
                                     textFieldHeight);
    
    self.dateDeparture.frame = CGRectMake(offset,
                                          self.cityTo.frame.origin.y + offset,
                                          (width - offset*2)*0.82,
                                          textFieldHeight);
   
    
    self.countOfTickets.frame = CGRectMake(self.dateDeparture.frame.origin.x + self.dateDeparture.frame.size.width + width*0.01f,
                                           self.cityTo.frame.origin.y + offset,
                                           buttonWidth*0.3f,
                                           textFieldHeight);
    
    self.filter.frame = CGRectMake(width/2 - buttonWidth*0.2f,
                                   self.dateDeparture.frame.origin.y + self.dateDeparture.frame.size.height,
                                   buttonWidth*0.4f,
                                   textFieldHeight);


    
    
    self.resultTable.frame = CGRectMake(0.0f,
                                        self.countOfTickets.frame.origin.y + self.countOfTickets.frame.size.height + offset,
                                        width,
                                        height - (self.countOfTickets.frame.origin.y + self.countOfTickets.frame.size.height + offset)+offset);//
    self.screenShot.frame = CGRectMake(0,
                                       0,
                                       width,
                                       self.countOfTickets.frame.origin.y);
    self.scroll.contentSize = CGSizeMake(width,self.resultTable.frame.origin.y + self.resultTable.frame.size.height);
}

@end

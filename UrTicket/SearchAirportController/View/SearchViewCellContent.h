//
//  SearchViewCellContent.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SearchViewCellContent : UIView

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *timeFromLabel;
@property (nonatomic, strong) UILabel *timeToLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *stopsLabel;
@property (nonatomic, strong) UILabel *countStops;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destinationLabel;


@property (nonatomic, strong) UIButton *addToLiked;

- (void)setItem:(id)item originName:(NSString *)orName destinationName:(NSString *)deName;

- (void)prepareForReuse;

@end

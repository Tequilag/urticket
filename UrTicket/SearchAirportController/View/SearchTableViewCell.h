//
//  SearchTableViewCell.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchViewCellContent.h"

@interface SearchTableViewCell : UITableViewCell

@property (nonatomic, strong) SearchViewCellContent *cellContentView;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(id)item originName:(NSString *)orName destinationName:(NSString *)deName;

@end

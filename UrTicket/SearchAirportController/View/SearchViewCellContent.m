//
//  SearchViewCellContent.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SearchViewCellContent.h"


NSDateFormatter * dateFormatFrom;
NSDateFormatter * dateFormatTo;
@implementation SearchViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        dateFormatFrom = [[NSDateFormatter alloc] init];
        [dateFormatFrom setDateFormat:@"yyyy-MM-dd"];
        dateFormatTo = [[NSDateFormatter alloc] init];
        [dateFormatTo setDateFormat:@"dd.MM.yy"];
        
        _priceLabel = [[UILabel alloc] init];
        //_priceLabel.layer.backgroundColor = [UIColor darkGrayColor].CGColor;
        _priceLabel.textAlignment = NSTextAlignmentLeft;
        _priceLabel.textColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
        _priceLabel.font = [UIFont systemFontOfSize:17];
        [self addSubview:_priceLabel];
        
        _timeFromLabel = [[UILabel alloc] init];
        //_timeFromLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _timeFromLabel.textColor =[UIColor blackColor];
        _timeFromLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeFromLabel];
        
        _originLabel = [[UILabel alloc] init];
        //_originLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        _originLabel.textColor =[UIColor blackColor];
        _originLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_originLabel];
        
        _destinationLabel = [[UILabel alloc] init];
        //_destinationLabel.layer.backgroundColor = [UIColor blueColor].CGColor;
        _destinationLabel.textColor =[UIColor blackColor];
        _destinationLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_destinationLabel];
        
        _timeToLabel = [[UILabel alloc] init];
        //_timeToLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _timeToLabel.textColor =[UIColor blackColor];
        _timeToLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeToLabel];
        
        _countStops = [[UILabel alloc] init];
        _countStops.text = @"-";
       // _countStops.layer.backgroundColor = [UIColor yellowColor].CGColor;
        _countStops.textAlignment = NSTextAlignmentCenter;
        _countStops.font = [UIFont systemFontOfSize:16];
        [self addSubview:_countStops];
        
        _stopsLabel = [[UILabel alloc] init];
        _stopsLabel.text = @"Пересадки:";
        //_stopsLabel.layer.backgroundColor = [UIColor orangeColor].CGColor;
        _stopsLabel.textColor =[UIColor blackColor];
        _stopsLabel.textAlignment = NSTextAlignmentCenter;
        _stopsLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_stopsLabel];
        
        _addToLiked = [[UIButton alloc] init];
        //_addToLiked.layer.backgroundColor = [UIColor brownColor].CGColor;
        [_addToLiked setImage:[UIImage imageNamed:@"Star"] forState:UIControlStateNormal];
        [self addSubview:_addToLiked];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
   
    CGFloat width = self.bounds.size.width;
    CGFloat offset = width * 0.05f;
    CGFloat widthPriceLabel = width*0.55f;
    CGFloat widthLabel = width*0.8f;
    CGFloat heightLabel = self.bounds.size.height * 0.1f;
    
    self.originLabel.frame = CGRectMake(offset,
                                        offset,
                                        widthLabel,
                                        heightLabel);
    
    self.timeFromLabel.frame = CGRectMake(offset,
                                          self.originLabel.frame.origin.y + self.originLabel.frame.size.height + offset,
                                          width*0.5f,
                                          heightLabel);
    
    self.destinationLabel.frame = CGRectMake(offset,
                                             self.timeFromLabel.frame.origin.y + self.timeFromLabel.frame.size.height + offset,
                                             widthLabel,
                                             heightLabel);
    
    self.timeToLabel.frame = CGRectMake(offset,
                                          self.destinationLabel.frame.origin.y + self.destinationLabel.frame.size.height + offset,
                                          widthLabel,
                                          heightLabel);
    
    self.priceLabel.frame = CGRectMake(offset,
                                       self.timeToLabel.frame.origin.y + self.timeToLabel.frame.size.height + offset,
                                       widthPriceLabel,
                                       heightLabel*1.3f);

    self.stopsLabel.frame = CGRectMake(self.priceLabel.frame.origin.x + self.priceLabel.frame.size.width,
                                       self.priceLabel.frame.origin.y,
                                       width*0.3f,
                                       heightLabel);
    
    self.countStops.frame = CGRectMake(self.stopsLabel.frame.origin.x + self.stopsLabel.frame.size.width,
                                       self.stopsLabel.frame.origin.y,
                                       width*0.1f,
                                       heightLabel);
//
    self.addToLiked.frame = CGRectMake(width - offset - width * 0.1f,
                                       offset,
                                       width * 0.1f,
                                       width * 0.1f);
}

- (void)setItem:(id)item originName:(NSString *)orName destinationName:(NSString *)deName {

    NSArray *segment = [item objectForKey:@"segment"];
   // NSArray *tempAr=[[item objectForKey:@"saleTotal"] componentsSeparatedByString:@"RUB"];
    if ([[[item objectForKey:@"saleTotal"] componentsSeparatedByString:@"RUB"] count] >1 ){
        self.priceLabel.text = [NSString stringWithFormat:@"%@ руб.",[[item objectForKey:@"saleTotal"] stringByReplacingOccurrencesOfString:@"RUB" withString:@""]];
    }
    else
        self.priceLabel.text = [NSString stringWithFormat:@"%@",[item objectForKey:@"saleTotal"]];
    
    self.originLabel.text = [NSString stringWithFormat:@"%@ (%@)",orName,[[segment firstObject] objectForKey:@"origin"] ];
    self.destinationLabel.text = [NSString stringWithFormat:@"%@ (%@)",deName,[[segment lastObject] objectForKey:@"destination"]];
    if ([segment count] > 1) {
        self.countStops.text = [NSString stringWithFormat:@"%i",(int)[segment count]/2];
    }
    //[[segment firstObject]
    
    NSArray *timeDep = [[NSString stringWithFormat:@"%@",[[segment firstObject] objectForKey:@"departureTime"]] componentsSeparatedByString:@"T"];
    self.timeFromLabel.text = [NSString stringWithFormat:@"Вылет:%@ %@",
                               [dateFormatTo stringFromDate:[dateFormatFrom dateFromString:timeDep[0]]],
                               [timeDep[1] componentsSeparatedByString:@"+"][0]];
    NSArray *timeArr = [[NSString stringWithFormat:@"%@",[[segment lastObject] objectForKey:@"arrivalTime"]] componentsSeparatedByString:@"T"];
    self.timeToLabel.text = [NSString stringWithFormat:@"Прибытие:%@ %@",
                             [dateFormatTo stringFromDate:[dateFormatFrom dateFromString:timeArr[0]]],
                             [timeArr[1] componentsSeparatedByString:@"+"][0]];
    [self setNeedsLayout];
}

- (void)prepareForReuse {
    self.countStops.text = @"-";
}

@end

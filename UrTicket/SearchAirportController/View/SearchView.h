//
//  SearchView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchView : UIView

@property (nonatomic, strong) UITableView *resultTable;

@property (nonatomic, strong) UITextField *cityFrom;
@property (nonatomic, strong) UITextField *cityTo;
@property (nonatomic, strong) UITextField *dateDeparture;
@property (nonatomic, strong) UITextField *dateArrive;

@property (nonatomic, strong) UIButton *countOfTickets;
@property (nonatomic, strong) UIButton *swapCities;
@property (nonatomic, strong) UIButton *filter;


@property (nonatomic, strong) UILabel *resultsLabel;

@property (nonatomic, retain) UIScrollView *scroll;
@property (nonatomic, strong) UIImageView *screenShot;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

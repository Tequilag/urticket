//
//  SearchTableViewCell.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 15.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SearchTableViewCell.h"

static CGFloat const ViewContentMargin = 8.0f;

@interface SearchTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end

@implementation SearchTableViewCell

+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[SearchTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor colorWithRed:222/255.0 green:227/255.0 blue:234/255.0 alpha:1];
        
        _contentInsets = UIEdgeInsetsMake(ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin);
        
        _cellContentView = [[SearchViewCellContent alloc] init];
        _cellContentView.layer.cornerRadius = 15.0f;
        _cellContentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:_cellContentView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.cellContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
}

-(void)setItem:(id)item originName:(NSString *)orName destinationName:(NSString *)deName{
    [self.cellContentView setItem:item originName:orName destinationName:deName];
}

- (void)prepareForReuse {
    //[super prepareForReuse];
    //[self.cellContentView prepareForReuse];
}


@end

//
//  TicketViewModel.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketViewModel : NSObject


@property (nonatomic) NSString* countOfAdults;
@property (nonatomic) NSString* countOfTeens;
@property (nonatomic) NSString* countOfChildren;
@property (nonatomic) NSString* prefferedCabin;
@property (nonatomic) NSString* airline;
@property (nonatomic) NSString *from;
@property (nonatomic) NSString *to;
@property (nonatomic) NSDate *dateOfRequest;
@property (nonatomic) NSDate *dateOfDeparture;

@end

//
//  TicketViewModel.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "TicketViewModel.h"

@implementation TicketViewModel

@synthesize countOfAdults;
@synthesize countOfTeens;
@synthesize countOfChildren;
@synthesize prefferedCabin;
@synthesize airline;
@synthesize from;
@synthesize to;
@synthesize dateOfRequest;
@synthesize dateOfDeparture;

@end

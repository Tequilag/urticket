//
//  TicketView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 27.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketView : UIView

@property (nonatomic, assign) CGFloat topOffset;

@property (nonatomic, strong) UIPickerView *adultpicker;
@property (nonatomic, strong) UIPickerView *teenPicker;
@property (nonatomic, strong) UIPickerView *childPicker;
@property (nonatomic, strong) UIPickerView *cabbinPicker;
@property (nonatomic, strong) UIPickerView *alliancePicker;

@property (nonatomic, strong) UIImageView *adultImage;
@property (nonatomic, strong) UIImageView *teenImage;
@property (nonatomic, strong) UIImageView *childImage;

@property (nonatomic, strong) UILabel *adultLabel;
@property (nonatomic, strong) UILabel *teenLabel;
@property (nonatomic, strong) UILabel *childLabel;
@property (nonatomic, strong) UILabel *cabbinLabel;
@property (nonatomic, strong) UILabel *allianceLabel;

@property(nonatomic, strong) UIView *countOfTickets;
@property(nonatomic, strong) UIView *preferredCabin;

@property(nonatomic, strong) UIButton *toCountOfTickets;
@property(nonatomic, strong) UIButton *toPreferredCabin;

@property  (nonatomic,strong) UIView *modalView;

@end

//
//  TicketView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 27.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "TicketView.h"

@implementation TicketView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];

    _countOfTickets = [[UIView alloc] init];
    _countOfTickets.backgroundColor = [UIColor whiteColor];
    [self addSubview:_countOfTickets];
    
    _preferredCabin = [[UIView alloc] init];
    _preferredCabin.backgroundColor = [UIColor whiteColor];
    _preferredCabin.hidden = YES;
    [self addSubview:_preferredCabin];
    
    
    _toCountOfTickets = [[UIButton alloc] init];
    _toCountOfTickets.tag = 1;
    _toCountOfTickets.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_toCountOfTickets setTitleColor: [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1] forState:UIControlStateNormal];
    [_toCountOfTickets.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_toCountOfTickets.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_toCountOfTickets setTitle:@"Количество билетов" forState:UIControlStateNormal];
    [self addSubview:_toCountOfTickets];
    
    _toPreferredCabin = [[UIButton alloc] init];
    _toPreferredCabin.tag = 2;
    _toPreferredCabin.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_toPreferredCabin setTitleColor: [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1] forState:UIControlStateNormal];
    [_toPreferredCabin.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_toPreferredCabin.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_toPreferredCabin setTitle:@"Параметры" forState:UIControlStateNormal];
    [self addSubview:_toPreferredCabin];
 
    _adultImage = [[UIImageView alloc] init];
    [_adultImage setImage:[UIImage imageNamed:@"Adult"]];
    [self.countOfTickets addSubview:_adultImage];
    
    _teenImage = [[UIImageView alloc] init];
    [_teenImage setImage:[UIImage imageNamed:@"Teen"]];
    [self.countOfTickets addSubview:_teenImage];
    
    _childImage = [[UIImageView alloc] init];
    [_childImage setImage:[UIImage imageNamed:@"Child"]];
    [self.countOfTickets addSubview:_childImage];
    
    _adultLabel = [[UILabel alloc] init];
    _adultLabel.textAlignment = NSTextAlignmentCenter;
    _adultLabel.text = @"Старше 12 лет";
    [_adultLabel setFont:[UIFont boldSystemFontOfSize:10]];
    [self.countOfTickets addSubview:_adultLabel];
    
    _teenLabel = [[UILabel alloc] init];
    _teenLabel.textAlignment = NSTextAlignmentCenter;
    _teenLabel.text = @"От 2 до 12 лет";
    [_teenLabel setFont:[UIFont boldSystemFontOfSize:10]];
    [self.countOfTickets addSubview:_teenLabel];
    
    _childLabel = [[UILabel alloc] init];
    _childLabel.textAlignment = NSTextAlignmentCenter;
    _childLabel.text = @"Младше 2 лет";
    [_childLabel setFont:[UIFont boldSystemFontOfSize:10]];
    [self.countOfTickets addSubview:_childLabel];
    
    _cabbinLabel = [[UILabel alloc] init];
    _cabbinLabel.textAlignment = NSTextAlignmentCenter;
    _cabbinLabel.text = @"Класс салона";
    [_cabbinLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.preferredCabin addSubview:_cabbinLabel];
    
    _allianceLabel = [[UILabel alloc] init];
    _allianceLabel.textAlignment = NSTextAlignmentCenter;
    _allianceLabel.text = @"Альянс";
    [_allianceLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [self.preferredCabin addSubview:_allianceLabel];
    
    _adultpicker = [[UIPickerView alloc] init];
    _adultpicker.tag = 100100;
    [self.countOfTickets addSubview:_adultpicker];
    
    _teenPicker = [[UIPickerView alloc] init];
    _teenPicker.tag = 100101;
    [self.countOfTickets addSubview:_teenPicker];
    
    _childPicker = [[UIPickerView alloc] init];
    _childPicker.tag = 100102;
    [self.countOfTickets addSubview:_childPicker];
    
    _cabbinPicker = [[UIPickerView alloc] init];
    _cabbinPicker.tag = 100103;
    [self.preferredCabin addSubview:_cabbinPicker];
    
    _alliancePicker = [[UIPickerView alloc] init];
    _alliancePicker.tag = 100104;
    [self.preferredCabin addSubview:_alliancePicker];
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height*0.75;
    CGFloat offset = width * 0.05f;
    CGFloat widthImageView = (width - offset*2.0f)/3.0f;
    
    self.toCountOfTickets.frame = CGRectMake(offset,
                                             offset,
                                             width*0.5f,
                                             20);
    self.toPreferredCabin.frame = CGRectMake(self.toCountOfTickets.frame.origin.x + self.toCountOfTickets.frame.size.width + offset,
                                             offset,
                                             width*0.4f,
                                             20);
   
    self.preferredCabin.frame = CGRectMake(0,
                                           self.toCountOfTickets.frame.origin.y + offset,
                                           width,
                                           height);
    
    self.countOfTickets.frame = CGRectMake(0,
                                           self.toCountOfTickets.frame.origin.y + offset,
                                           width,
                                           height);
    
    self.adultImage.frame = CGRectMake(offset,
                                       offset,
                                       widthImageView,
                                       height*0.2f);
    
    self.adultLabel.frame = CGRectMake(offset + self.adultImage.frame.size.width/2 - widthImageView/2,
                                       self.adultImage.frame.origin.y + self.adultImage.frame.size.height + width*0.01f,
                                       widthImageView,
                                       20);
    
    self.teenLabel.frame = CGRectMake(self.adultLabel.frame.origin.x + widthImageView,
                                      self.teenImage.frame.origin.y + self.teenImage.frame.size.height + width*0.01f,
                                      widthImageView,
                                      20);
    
    self.childLabel.frame = CGRectMake(self.teenLabel.frame.origin.x + widthImageView,
                                       self.childImage.frame.origin.y + self.childImage.frame.size.height + width*0.01f,
                                       widthImageView,
                                       20);
    
    self.teenImage.frame = CGRectMake(width*0.5f - widthImageView*0.25f,
                                      offset + self.adultImage.frame.size.height*0.25f,
                                      widthImageView*0.5f,
                                      height*0.15f);
    
    self.childImage.frame = CGRectMake(self.childLabel.frame.origin.x + self.childLabel.frame.size.width*0.5f - widthImageView * 0.125f,
                                       offset + self.adultImage.frame.size.height*0.5f,
                                       widthImageView * 0.25f,
                                       height*0.1f);
    
    self.adultpicker.frame = CGRectMake(offset,
                                        self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height,
                                        widthImageView,
                                        height - (self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height));
    
    self.teenPicker.frame = CGRectMake(self.adultpicker.frame.origin.x + widthImageView,
                                        self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height,
                                        widthImageView,
                                        height - (self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height));
    
    self.childPicker.frame = CGRectMake(self.teenPicker.frame.origin.x + widthImageView,
                                       self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height,
                                       widthImageView,
                                       height - (self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height));
    
    self.childPicker.frame = CGRectMake(self.teenPicker.frame.origin.x + widthImageView,
                                        self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height,
                                        widthImageView,
                                        height - (self.teenLabel.frame.origin.y + self.teenLabel.frame.size.height));
    
    self.cabbinLabel.frame = CGRectMake(offset,
                                         offset,
                                         widthImageView * 1.5f,
                                         20);
    
    self.allianceLabel.frame = CGRectMake(width - offset - widthImageView * 1.5f,
                                        offset,
                                        widthImageView * 1.5f,
                                        20);
    self.cabbinPicker.frame = CGRectMake(self.cabbinLabel.frame.origin.x,
                                        self.cabbinLabel.frame.origin.y + self.cabbinLabel.frame.size.height,
                                        self.cabbinLabel.frame.size.width,
                                        height - (self.cabbinLabel.frame.origin.y + self.cabbinLabel.frame.size.height));
    
    self.alliancePicker.frame = CGRectMake(self.allianceLabel.frame.origin.x,
                                         self.allianceLabel.frame.origin.y + self.allianceLabel.frame.size.height,
                                         self.allianceLabel.frame.size.width,
                                         height - (self.allianceLabel.frame.origin.y + self.allianceLabel.frame.size.height));
    self.modalView.frame =CGRectMake(30,
                                     150,
                                     290,
                                     250);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}
@end

//
//  TicketViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 27.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketViewModel.h"

@protocol TicketViewControllerDelegate;

@interface TicketViewController : UIViewController

@property (nonatomic, weak) id<TicketViewControllerDelegate> delegate;
@property (nonatomic, strong) TicketViewModel *ticketViewModel;
@property (nonatomic) BOOL mainOrSearch;

@end

@protocol TicketViewControllerDelegate <NSObject>

-(void)setTicket:(NSObject *)value;

@end

//
//  TicketViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 27.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "TicketViewController.h"
#import "TicketView.h"
#import "TicketViewModel.h"

@interface TicketViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

@property (nonatomic, strong) TicketView* ticketView;
@property (nonatomic, strong) UITapGestureRecognizer *gesture;

@end

NSArray *adults;
NSArray *teens;
NSArray *children;
NSArray *preferredCabbin;
NSArray *alliance;
@implementation TicketViewController

- (void)loadView {
    [super loadView];
    self.ticketView = [[TicketView alloc]init];
    self.view = self.ticketView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.ticketView.toPreferredCabin addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.ticketView.toCountOfTickets addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    
    adults = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"];
    teens = @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8"];
    children = @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8"];
    preferredCabbin = @[@"Эконом", @"Премиум", @"Бизнес", @"Первый"];
    alliance = @[@"Oneworld", @"SkyTeam", @"Star"];
    
    self.ticketView.adultpicker.delegate = self;
    self.ticketView.adultpicker.dataSource = self;
    
    self.ticketView.teenPicker.delegate = self;
    self.ticketView.teenPicker.dataSource = self;
    
    self.ticketView.childPicker.delegate = self;
    self.ticketView.childPicker.dataSource = self;
    
    self.ticketView.alliancePicker.delegate = self;
    self.ticketView.alliancePicker.dataSource = self;
    
    self.ticketView.cabbinPicker.delegate = self;
    self.ticketView.cabbinPicker.dataSource = self;
    
    if (self.ticketViewModel){
        [self.ticketView.adultpicker selectRow:([self.ticketViewModel.countOfAdults intValue] - 1) inComponent:0 animated:YES];
        [self.ticketView.teenPicker selectRow:[self.ticketViewModel.countOfTeens intValue]  inComponent:0 animated:YES];
        [self.ticketView.childPicker selectRow:[self.ticketViewModel.countOfChildren intValue]  inComponent:0 animated:YES];
        [self.ticketView.cabbinPicker selectRow:[preferredCabbin indexOfObject:self.ticketViewModel.prefferedCabin]  inComponent:0 animated:YES];
        [self.ticketView.alliancePicker selectRow:[alliance indexOfObject:self.ticketViewModel.airline]  inComponent:0 animated:YES];
    }
    else
        self.ticketViewModel = [[TicketViewModel alloc] init];
    if (_mainOrSearch){
        self.ticketView.adultpicker.userInteractionEnabled = NO;
        self.ticketView.teenPicker.userInteractionEnabled = NO;
        self.ticketView.childPicker.userInteractionEnabled = NO;
        self.ticketView.preferredCabin.userInteractionEnabled = NO;
        self.ticketView.alliancePicker.userInteractionEnabled = NO;
    }
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.gesture = [[UITapGestureRecognizer alloc] init];
    self.gesture.numberOfTapsRequired = 1;
    self.gesture.numberOfTouchesRequired = 1;
    self.gesture.delegate = self;
    [self.gesture setCancelsTouchesInView:NO];
    [self.view.window addGestureRecognizer:self.gesture];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:nil];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication]
                             delegate] window];
    CGPoint pointInSubview = [self.view convertPoint:point
                                            fromView:mainWindow];
    if (!CGRectContainsPoint(self.view.frame, pointInSubview)) {
        self.gesture.delegate = nil;
        NSInteger row;
        
        //TicketViewModel *ticketViewModel = [[TicketViewModel alloc] init];
        
        row = [self.ticketView.adultpicker selectedRowInComponent:0];
        self.ticketViewModel.countOfAdults = [adults objectAtIndex:row];
        row = [self.ticketView.teenPicker selectedRowInComponent:0];
        self.ticketViewModel.countOfTeens = [teens objectAtIndex:row];
        row = [self.ticketView.childPicker selectedRowInComponent:0];
        self.ticketViewModel.countOfChildren = [children objectAtIndex:row];
        row = [self.ticketView.cabbinPicker selectedRowInComponent:0];
        self.ticketViewModel.prefferedCabin = [preferredCabbin objectAtIndex:row];
        row = [self.ticketView.alliancePicker selectedRowInComponent:0];
        self.ticketViewModel.airline = [alliance objectAtIndex:row];
        [self.delegate setTicket:self.ticketViewModel];
        [self.view.window removeGestureRecognizer:self.gesture];
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (pickerView.tag) {
        case 100100:
            return  adults.count;
            break;
        case 100101:
            return  teens.count;
            break;
        case 100102:
            return  children.count;
            break;
        case 100103:
            return  preferredCabbin.count;
            break;
        case 100104:
            return  alliance.count;
            break;
    }
    return adults.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (pickerView.tag) {
        case 100100: return adults[row];
            break;
        case 100101: return teens[row];
            break;
        case 100102: return children[row];
            break;
        case 100103: return preferredCabbin[row];
            break;
        case 100104: return alliance[row];
            break;
    }
    return adults[row];
}

#pragma for ask
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    UILabel *title = (UILabel *)view;
//    if (!title)
//        title = [[UILabel alloc] init];
//    switch (pickerView.tag) {
//        case 1: title.text = (NSString *)adults[row];
//            break;
//        case 2: title.text = (NSString *)teens[row];
//            break;
//        case 3: title.text =  (NSString *)children[row];
//            break;
//        case 4: {
//            title.text =  (NSString *)preferredCabbin[row];
//            title.font = [UIFont boldSystemFontOfSize:10];
//        }
//            break;
//        case 5:{
//            title.text =  (NSString *)alliance[row];
//        }
//            break;
//    }
//    return title;
//}

-(void)changeView:(UIButton *)sender{
    if (sender.tag == 1) {
        self.ticketView.preferredCabin.hidden = YES;
        self.ticketView.countOfTickets.hidden = NO;
    }
    else {
        self.ticketView.preferredCabin.hidden = NO;
        self.ticketView.countOfTickets.hidden = YES;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

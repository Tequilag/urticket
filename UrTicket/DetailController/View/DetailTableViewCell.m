//
//  DetailTableViewCell.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 03.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "DetailTableViewCell.h"

static CGFloat const ViewContentMargin = 0.0f;

@interface DetailTableViewCell ()

@property (nonatomic, assign) UIEdgeInsets contentInsets;

@end

@implementation DetailTableViewCell


+ (NSString *)reuseIdentifier {
    return NSStringFromClass(self);
}

- (instancetype)init {
    return [self initWithReuseIdentifier:[DetailTableViewCell reuseIdentifier]];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor colorWithRed:222/255.0 green:227/255.0 blue:234/255.0 alpha:1];
        
        _contentInsets = UIEdgeInsetsMake(ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin,
                                          ViewContentMargin);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
        if (_typeOfDetailViewCellContent == DetailViewCellContentTypeNormal)
            self.cellContentView.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
        else
        {
            self.cellContentViewCD.frame = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
        }
}

-(void)setItem:(id)item{
    if (_typeOfDetailViewCellContent == DetailViewCellContentTypeNormal)
        [self.cellContentView setItem:item];
    else
        [self.cellContentViewCD setItem:item];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    //[self.cellContentView prepareForReuse];
}

-(void)addNormalView{
    _cellContentView = [[DetailViewCellContent alloc] init];
    //_cellContentView.layer.cornerRadius = 10.0f;
    _cellContentView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_cellContentView];
}

-(void)addCdView{
    _cellContentViewCD = [[DetailViewCellContentCD alloc] init];
   // _cellContentViewCD.layer.cornerRadius = 10.0f;
    _cellContentViewCD.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_cellContentViewCD];
}
@end

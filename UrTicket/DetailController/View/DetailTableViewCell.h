//
//  DetailTableViewCell.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 03.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewCellContent.h"
#import "DetailViewCellContentCD.h"

typedef enum : NSUInteger {
    DetailViewCellContentTypeNormal,
    DetailViewControllerTypeCD,
} DetailViewCellContentType;

@interface DetailTableViewCell : UITableViewCell

@property (nonatomic, strong) DetailViewCellContent *cellContentView;
@property (nonatomic, strong) DetailViewCellContentCD *cellContentViewCD;
@property (nonatomic, assign) DetailViewCellContentType typeOfDetailViewCellContent;

+ (NSString *)reuseIdentifier;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

-(void)setItem:(id)item;
-(void)addNormalView;
-(void)addCdView;
@end

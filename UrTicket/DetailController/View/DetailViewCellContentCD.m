//
//  DetailViewCellContentCD.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 05.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "DetailViewCellContentCD.h"
#import "DetailViewModel.h"

@implementation DetailViewCellContentCD

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _timeLabel = [[UILabel alloc] init];
        //_timeLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _timeLabel.textColor =[UIColor blackColor];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeLabel];
        
        _originLabel = [[UILabel alloc] init];
        //_originLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        _originLabel.textColor =[UIColor blackColor];
        _originLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_originLabel];
        
//        _nameLabel = [[UILabel alloc] init];
//       // _nameLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
//        _nameLabel.textColor =[UIColor blackColor];
//        _nameLabel.font = [UIFont systemFontOfSize:14];
//        _nameLabel.text = @"Пересадка";
//        [self addSubview:_nameLabel];
        
        _line1 = [[UILabel alloc] init];
        _line1.layer.backgroundColor = [UIColor blackColor].CGColor;
        [self addSubview:_line1];
        
        _line2 = [[UILabel alloc] init];
        _line2.layer.backgroundColor = [UIColor blackColor].CGColor;
        [self addSubview:_line2];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat width = self.bounds.size.width;
    CGFloat offset = width * 0.05f;
    CGFloat widthLabel = width*0.5f;
    CGFloat heightLabel = self.bounds.size.height * 0.3f;
    
    
    
//    self.nameLabel.frame = CGRectMake(self.timeLabel.frame.origin.x + self.timeLabel.frame.size.width + offset,
//                                        offset,
//                                        widthLabel,
//                                        heightLabel);
    
    self.originLabel.frame = CGRectMake(offset,
                                        offset,
                                        width,
                                        heightLabel);
    
    self.timeLabel.frame = CGRectMake(offset,
                                      self.originLabel.frame.origin.y + self.originLabel.frame.size.height,
                                      width,
                                      heightLabel);
    self.line1.frame = CGRectMake(0,
                                  0,
                                  width,
                                  self.bounds.size.height*0.02f);
    
    self.line2.frame = CGRectMake(0,
                                  self.bounds.size.height - self.bounds.size.height*0.02f,
                                  width,
                                  self.bounds.size.height*0.02f);
}

- (void)setItem:(id)item{
    DetailViewModel *detainViewModel = [[DetailViewModel alloc] init];
    
    self.originLabel.text = [NSString stringWithFormat:@"Пересадка: %@ (%@)",
                             [detainViewModel getCityNameByCityCode:[detainViewModel getCityCodeByAirportCode:[item valueForKey:@"origin"]]],
                              [item valueForKey:@"origin"]];
    long temp =[[item valueForKey:@"connectionDuration"] longValue];
    self.timeLabel.text = [NSString stringWithFormat:@"Длительность пересадки: %lu ч %lu мин", temp/60 , temp - 60*(temp/60)];
    [self setNeedsLayout];
}

@end

//
//  DetailView.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailView.h"

@implementation DetailView
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.scrollEnabled = YES;
    [_scrollView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:_scrollView];
    
    _mapView = [[MKMapView alloc] init];
    [self.scrollView addSubview:_mapView];
    
    _allTravel = [[UILabel alloc] init];
    _allTravel.text = @"Весь маршрут:";
    [self.scrollView  addSubview:_allTravel];
    
    _travelTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _travelTable.backgroundColor = [UIColor whiteColor];
    _travelTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _travelTable.scrollEnabled = YES;
    [self.scrollView  addSubview:_travelTable];
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat offset = width*0.05f;
    
    self.contentSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    
    self.scrollView.frame = CGRectMake(0,
                                       0,
                                       width,
                                       height);
    self.mapView.frame = CGRectMake(offset,
                                    offset,
                                    width - offset*2.0f,
                                    height/2.0f);
    
    self.allTravel.frame = CGRectMake(offset,
                                      self.mapView.frame.origin.y + self.mapView.bounds.size.height + offset,
                                      width,
                                      20.0f);
    
    self.travelTable.frame = CGRectMake(0,
                                        self.allTravel.frame.origin.y + self.allTravel.bounds.size.height + offset,
                                        width,
                                        height);// - (self.allTravel.frame.origin.y + self.allTravel.frame.size.height + offset)+offset);
    
     self.scrollView.contentSize = CGSizeMake(width,self.travelTable.frame.origin.y + self.travelTable.frame.size.height);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end

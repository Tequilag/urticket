//
//  DetailViewCellContent.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 03.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewCellContent : UIView

@property (nonatomic, strong) UILabel *timeFromLabel;
@property (nonatomic, strong) UILabel *timeToLabel;
@property (nonatomic, strong) UILabel *dateFromLabel;
@property (nonatomic, strong) UILabel *dateToLabel;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destinationLabel;
@property (nonatomic, strong) UILabel *flight;
@property (nonatomic, strong) UILabel *airline;

- (void)setItem:(id)item;

@end

//
//  DetailViewCellContentCD.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 05.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewCellContentCD : UIView

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *line1;
@property (nonatomic, strong) UILabel *line2;

- (void)setItem:(id)item;

@end

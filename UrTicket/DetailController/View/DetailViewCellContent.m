//
//  DetailViewCellContent.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 03.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "DetailViewCellContent.h"
#import "DetailViewModel.h"

NSDateFormatter *dateFormatFromD;
NSDateFormatter *dateFormatToD;
NSString *qwertgg;

@implementation DetailViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        dateFormatFromD = [[NSDateFormatter alloc] init];
        [dateFormatFromD setDateFormat:@"yyyy-MM-dd"];
        dateFormatToD = [[NSDateFormatter alloc] init];
        [dateFormatToD setDateFormat:@"dd.MM.yy"];
        qwertgg = [[NSString alloc] init];
        
        
        _originLabel = [[UILabel alloc] init];
       // _originLabel.layer.backgroundColor = [UIColor redColor].CGColor;
        _originLabel.textColor =[UIColor blackColor];
        _originLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_originLabel];
        
        _destinationLabel = [[UILabel alloc] init];
       // _destinationLabel.layer.backgroundColor = [UIColor blueColor].CGColor;
        _destinationLabel.textColor =[UIColor blackColor];
        _destinationLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_destinationLabel];
        
        _timeToLabel = [[UILabel alloc] init];
        //_timeToLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _timeToLabel.textColor =[UIColor blackColor];
        _timeToLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeToLabel];
        
        _timeFromLabel = [[UILabel alloc] init];
        //_timeFromLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _timeFromLabel.textColor =[UIColor blackColor];
        _timeFromLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_timeFromLabel];
        
        _dateFromLabel = [[UILabel alloc] init];
       // _dateFromLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _dateFromLabel.textColor =[UIColor blackColor];
        _dateFromLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_dateFromLabel];
        
        _dateToLabel = [[UILabel alloc] init];
       // _dateToLabel.layer.backgroundColor = [UIColor greenColor].CGColor;
        _dateToLabel.textColor =[UIColor blackColor];
        _dateToLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_dateToLabel];
        
        _flight = [[UILabel alloc] init];
       // _flight.layer.backgroundColor = [UIColor greenColor].CGColor;
        _flight.textColor =[UIColor blackColor];
        _flight.font = [UIFont systemFontOfSize:14];
        [self addSubview:_flight];
        
        _airline = [[UILabel alloc] init];
        _airline.lineBreakMode = NSLineBreakByWordWrapping;
        _airline.numberOfLines = 0;
        _airline.textColor =[UIColor blackColor];
        _airline.font = [UIFont systemFontOfSize:14];
        [self addSubview:_airline];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat width = self.bounds.size.width;
    CGFloat offset = width * 0.05f;
    CGFloat widthLabel = width*0.5f;
    CGFloat heightLabel = self.bounds.size.height * 0.1f;
    
    self.dateFromLabel.frame = CGRectMake(offset,
                                        offset,
                                        widthLabel,
                                        heightLabel);
    
    self.originLabel.frame = CGRectMake(offset,
                                             self.dateFromLabel.frame.origin.y + self.dateFromLabel.frame.size.height,
                                             widthLabel,
                                             heightLabel);
    
    self.dateToLabel.frame = CGRectMake(self.originLabel.frame.origin.x + self.originLabel.frame.size.width + offset,
                                          offset,
                                          widthLabel,
                                          heightLabel);
    
    self.destinationLabel.frame = CGRectMake(self.dateToLabel.frame.origin.x,
                                             self.dateFromLabel.frame.origin.y + self.dateFromLabel.frame.size.height,
                                             widthLabel,
                                             heightLabel);
    
    self.timeFromLabel.frame = CGRectMake(offset,
                                          self.originLabel.frame.origin.y + self.originLabel.frame.size.height + offset,
                                          width*0.3f,
                                          heightLabel);
    
    self.timeToLabel.frame = CGRectMake(self.destinationLabel.frame.origin.x,
                                        self.destinationLabel.frame.origin.y + self.destinationLabel.frame.size.height + offset,
                                        width*0.3f,
                                        heightLabel);
    self.flight.frame = CGRectMake(offset,
                                   self.timeFromLabel.frame.origin.y + self.timeFromLabel.frame.size.height + offset,
                                   width*0.4f,
                                   heightLabel);
    
    self.airline.frame = CGRectMake(offset,
                                    self.flight.frame.origin.y + self.flight.frame.size.height + offset,
                                    width - offset*2.0f,
                                    heightLabel);
}

- (void)setItem:(id)item{
    DetailViewModel *detailViewModel = [[DetailViewModel alloc] init];
    NSString *part1 =[detailViewModel getCityNameByCityCode:[detailViewModel getCityCodeByAirportCode:[item valueForKey:@"origin"]]];
    NSString *part2 = [item valueForKey:@"origin"];
    self.originLabel.text = [NSString stringWithFormat:@"%@ (%@)",part1, part2];
    self.destinationLabel.text = [NSString stringWithFormat:@"%@ (%@)",[detailViewModel getCityNameByCityCode:[detailViewModel getCityCodeByAirportCode:[item valueForKey:@"destination"]]],[item valueForKey:@"destination"]];
   
    NSArray *timeDep = [[NSString stringWithFormat:@"%@",[item valueForKey:@"departureTime"]] componentsSeparatedByString:@"T"];
    self.timeFromLabel.text = [NSString stringWithFormat:@"%@", [timeDep[1] componentsSeparatedByString:@"+"][0]] ;
    self.dateFromLabel.text = [NSString stringWithFormat:@"%@", [dateFormatToD stringFromDate:[dateFormatFromD dateFromString:timeDep[0]]]];
    
    NSArray *timeArr = [[NSString stringWithFormat:@"%@",[item valueForKey:@"arrivalTime"]] componentsSeparatedByString:@"T"];
    self.timeToLabel.text = [NSString stringWithFormat:@"%@", [timeArr[1] componentsSeparatedByString:@"+"][0]];
    self.dateToLabel.text = [NSString stringWithFormat:@"%@", [dateFormatToD stringFromDate:[dateFormatFromD dateFromString:timeArr[0]]]];
    [self setNeedsLayout];
    self.flight.text = [NSString stringWithFormat:@"Рейс: %@",[item valueForKey:@"flight"]];
    if ([item valueForKey:@"operatingDisclosure"])
        self.airline.text = [NSString stringWithFormat:@"Авиакомпания: %@",[[item valueForKey:@"operatingDisclosure"] componentsSeparatedByString:@"OPERATED BY"][1]];
    else
        self.airline.text = @"";
}

@end

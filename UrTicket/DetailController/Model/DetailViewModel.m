//
//  DetailViewModel.m
//  AirlineService
//
//  Created by Андрей on 23.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailViewModel.h"
#import "CoreData/CoreData.h"
#import "Airports+CoreDataClass.h"
#import "Cities+CoreDataClass.h"

#import "AppDelegate.h"

@interface DetailViewModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation DetailViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}


- (CLLocationCoordinate2D)getLocationAirportByCode:(NSString *)code {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",code];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Airports *resObj = nil;
    if ([result count] > 0) resObj = [result firstObject];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(resObj.latitude,resObj.longitude);
    return coordinate;
}

- (void)getAirportsByCityCode:(NSString *)cityCode {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",cityCode];
    request.predicate = predicate;
    
    self.airports = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
}

- (NSString *)getCityNameByCityCode:(NSString *)cityCode {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",cityCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *cityObj = nil;
    if ([result count] > 0) cityObj = [result firstObject];
    return cityObj.name;
}

- (NSString *)getCityCodeByAirportCode:(NSString *)airportCode {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",airportCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Airports *airportObj = nil;
    if ([result count] > 0) airportObj = [result firstObject];
    return airportObj.cityCode;
}
@end

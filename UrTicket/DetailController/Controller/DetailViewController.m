//
//  DetailViewController.m
//  AirlineService
//
//  Created by Андрей on 21.12.16.
//  Copyright © 2016 Home. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailView.h"
#import "MapKit/Mapkit.h"
#import "DetailViewModel.h"
#import "DetailTableViewCell.h"

static NSString *const ViewControllerMapViewPin = @"ViewControllerMapViewPin";

@interface DetailViewController () <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) DetailView *detailView;
@property (nonatomic,strong) DetailViewModel *detailViewModel;
@property (nonatomic, strong) MKPolyline *routeLine;

@end

CGFloat widthD;
CGFloat heightD;

@implementation DetailViewController

- (void)loadView {
    self.detailView = [[DetailView alloc] init];
    self.view = self.detailView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Детали";
    //self.navigationController.navigationBar.topItem.title = @"Назад";
    //self.navigationItem.leftBarButtonItem.title = @""
//    UIBarButtonItem *newBackButton =
//    [[UIBarButtonItem alloc] initWithTitle:@"Назад"
//                                     style:UIBarButtonItemStylePlain
//                                    target:nil
//                                    action:nil];
//    [[self navigationItem] setBackBarButtonItem:newBackButton];
    self.detailView.mapView.delegate = self;
    self.detailView.travelTable.delegate = self;
    self.detailView.travelTable.dataSource = self;
    
    self.detailViewModel = [[DetailViewModel alloc] init];
    [self createRoute];
}

//-(void)viewDidLayoutSubviews:(BOOL)animated{
//    [super viewDidLayoutSubviews];
//    widthD = self.detailView.frame.size.width;
//    heightD = self.detailView.frame.size.height;
//}
- (void)createRoute {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    CLLocationCoordinate2D coordinateArray[2];
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    MKPolyline *routeLine;
    for (NSInteger i = 0; i<[segment count];i++) {
        if ([segment[i] objectForKey:@"connectionDuration"]) continue;
       coordinateArray[0] = [self.detailViewModel getLocationAirportByCode:[segment[i] objectForKey:@"origin"]];
       coordinateArray[1] = [self.detailViewModel getLocationAirportByCode:[segment[i] objectForKey:@"destination"]];
       routeLine = [MKPolyline polylineWithCoordinates:coordinateArray count:2];
       [self.detailView.mapView addOverlay:routeLine];
        MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
        [pointAnnotation setCoordinate:coordinateArray[0]];
        [annotations addObject:pointAnnotation];
    }
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
    [pointAnnotation setCoordinate:coordinateArray[1]];
    [annotations addObject:pointAnnotation];
    [self.detailView.mapView addAnnotations:annotations];
    [self.detailView.mapView showAnnotations:annotations animated:YES];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *render = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        render.strokeColor = [UIColor redColor];
        render.lineWidth = 3;
        return render;
    }
    return nil;
}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:ViewControllerMapViewPin];
    if (!pinView) {
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                               reuseIdentifier:ViewControllerMapViewPin];
    }
    
    pinView.image = [UIImage imageNamed:@"airplane_black"];
    return pinView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    return [segment count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    if ([[segment objectAtIndex:indexPath.row]objectForKey:@"connectionDuration"])
    return 90.0f;
    else return 170.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *segment = [self.resultItem objectForKey:@"segment"];
    
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[DetailTableViewCell reuseIdentifier]];
    
    if (!cell) {
        cell = [[DetailTableViewCell alloc] init];
    }
    if ([[segment objectAtIndex:indexPath.row]objectForKey:@"connectionDuration"]){
        cell.typeOfDetailViewCellContent = DetailViewControllerTypeCD;
        [cell addCdView];
    }
    else{
        cell.typeOfDetailViewCellContent = DetailViewCellContentTypeNormal;
        [cell addNormalView];
    }
    [cell setItem:[segment objectAtIndex:indexPath.row]];//self.resultItem];
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.detailView.topOffset = self.topLayoutGuide.length;
}

@end

//
//  MainTicketView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 24.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTicketView : UIView

@property (nonatomic, strong) UIView *modalView;

@property (nonatomic, strong) UILabel *dateOfRequest;
@property (nonatomic, strong) UILabel *countOfAdults;
@property (nonatomic, strong) UILabel *countOfTeens;
@property (nonatomic, strong) UILabel *countOfChildren;
@property (nonatomic, strong) UILabel *prefferedCabbin;
@property (nonatomic, strong) UILabel *airline;
@property (nonatomic, strong) UILabel *labelFrom;
@property (nonatomic, strong) UILabel *labelTo;

@end

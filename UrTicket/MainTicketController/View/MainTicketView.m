//
//  MainTicketView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 24.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainTicketView.h"

@implementation MainTicketView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    
    _modalView = [[UIView alloc] init];
    _modalView.layer.backgroundColor = [UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0].CGColor;
    _modalView.layer.cornerRadius = 7;
    [self addSubview:_modalView];

    _countOfAdults = [[UILabel alloc] init];
    _countOfAdults.text = @"_countOfAdults";
    [_countOfAdults setFont:[UIFont boldSystemFontOfSize:13]];
    _countOfAdults.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_countOfAdults];
    
    _countOfTeens = [[UILabel alloc] init];
    _countOfTeens.text = @"_countOfTeens";
    [_countOfTeens setFont:[UIFont boldSystemFontOfSize:13]];
    _countOfTeens.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_countOfTeens];
    
    _countOfChildren = [[UILabel alloc] init];
    _countOfChildren.text = @"_countOfChildren";
    [_countOfChildren setFont:[UIFont boldSystemFontOfSize:13]];
    _countOfChildren.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_countOfChildren];
    
    _prefferedCabbin = [[UILabel alloc] init];
    _prefferedCabbin.text = @"_prefferedCabbin";
    [_prefferedCabbin setFont:[UIFont boldSystemFontOfSize:13]];
    _prefferedCabbin.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_prefferedCabbin];
    
    _airline = [[UILabel alloc] init];
    _airline.text = @"_airline";
    [_airline setFont:[UIFont boldSystemFontOfSize:13]];
    _airline.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_airline];
    
    _labelFrom = [[UILabel alloc] init];
    _labelFrom.text = @"_labelFrom";
    [_labelFrom setFont:[UIFont boldSystemFontOfSize:13]];
    _labelFrom.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_labelFrom];
    
    _labelTo = [[UILabel alloc] init];
    _labelTo.text = @"_labelTo";
    [_labelTo setFont:[UIFont boldSystemFontOfSize:13]];
    _labelTo.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_labelTo];
    
    _dateOfRequest = [[UILabel alloc] init];
    _dateOfRequest.text = @"_dateOfRequest";
    [_dateOfRequest setFont:[UIFont boldSystemFontOfSize:13]];
    _dateOfRequest.layer.backgroundColor = [UIColor clearColor].CGColor;
    [self.modalView addSubview:_dateOfRequest];
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.modalView.frame = CGRectMake(self.bounds.size.width * 0.1,
                                      self.bounds.size.height * 0.15,
                                      self.bounds.size.width * 0.8,
                                      self.bounds.size.height * 0.7);
    
    CGFloat width = self.modalView.frame.size.width;
    CGFloat offset = width * 0.05f;
    CGFloat labelLength = width - offset*2.0f;
    CGFloat labelHeight = self.modalView.frame.size.height * 0.09;
    
    self.dateOfRequest.frame = CGRectMake(offset,
                                          offset,
                                          labelLength,
                                          labelHeight);
    
    self.labelFrom.frame = CGRectMake(offset,
                                    self.dateOfRequest.frame.origin.y + labelHeight + offset,
                                    labelLength,
                                    labelHeight);
    
    self.labelTo.frame = CGRectMake(offset,
                                      self.labelFrom.frame.origin.y + labelHeight + offset,
                                      labelLength,
                                      labelHeight);
    
    self.countOfAdults.frame = CGRectMake(offset,
                                      self.labelTo.frame.origin.y + labelHeight + offset,
                                      labelLength,
                                      labelHeight);
    
    self.countOfTeens.frame = CGRectMake(offset,
                                         self.countOfAdults.frame.origin.y + labelHeight + offset,
                                         labelLength,
                                         labelHeight);
    
    self.countOfChildren.frame = CGRectMake(offset,
                                            self.countOfTeens.frame.origin.y + labelHeight + offset,
                                            labelLength,
                                            labelHeight);
    
    self.prefferedCabbin.frame = CGRectMake(offset,
                                      self.countOfChildren.frame.origin.y + labelHeight + offset,
                                      labelLength,
                                      labelHeight);
    
    self.airline.frame = CGRectMake(offset,
                                            self.prefferedCabbin.frame.origin.y + labelHeight + offset,
                                            labelLength,
                                            labelHeight);
    self.modalView.layer.masksToBounds = NO;
    self.modalView.layer.shadowOffset = CGSizeMake(0, 0);
    self.modalView.layer.shadowRadius = 50;
    self.modalView.layer.shadowOpacity = 1;
    
}

@end

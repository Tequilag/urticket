//
//  MainTicketViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 24.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainTicketViewController.h"
#import "MainTicketView.h"

@interface MainTicketViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) MainTicketView *mainTicketView;

@end

@implementation MainTicketViewController

-(void)loadView{
    [super loadView];
    self.mainTicketView = [[MainTicketView alloc] init];
    self.view = self.mainTicketView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCheck:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    NSDateFormatter *dateFormatTo = [[NSDateFormatter alloc] init];
    [dateFormatTo setDateFormat:@"dd.MM.yy"];
    
    self.mainTicketView.countOfAdults.text = [NSString stringWithFormat:@"Число взрослых:%@",_ticketViewModel.countOfAdults];
    self.mainTicketView.countOfTeens.text = [NSString stringWithFormat:@"Число подростков:%@",_ticketViewModel.countOfTeens];
    self.mainTicketView.countOfChildren.text =[NSString stringWithFormat:@"Число детей:%@", _ticketViewModel.countOfChildren];
    self.mainTicketView.dateOfRequest.text = [NSString stringWithFormat:@"Дата вылета:%@",[dateFormatTo stringFromDate:_ticketViewModel.dateOfDeparture]];
    self.mainTicketView.prefferedCabbin.text = [NSString stringWithFormat:@"Класс салона:%@",_ticketViewModel.prefferedCabin];
    self.mainTicketView.airline.text = [NSString stringWithFormat:@"Альянс:%@",_ticketViewModel.airline];
    self.mainTicketView.labelFrom.text = [NSString stringWithFormat:@"Откуда:%@",_ticketViewModel.from];
    self.mainTicketView.labelTo.text = [NSString stringWithFormat:@"Куда:%@",_ticketViewModel.to];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tapCheck:(UITapGestureRecognizer *)sender {
    if (!CGRectContainsPoint(self.mainTicketView.modalView.frame, [sender locationInView:self.view])){
        [self.view removeGestureRecognizer:sender];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

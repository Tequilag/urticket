//
//  MainTicketViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 24.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketViewModel.h"

@interface MainTicketViewController : UIViewController

@property (nonatomic, strong) TicketViewModel *ticketViewModel;

@end

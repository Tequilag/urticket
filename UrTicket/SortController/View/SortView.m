//
//  SortView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 10.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SortView.h"

@implementation SortView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    
    _sortTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _sortTable.backgroundColor = [UIColor whiteColor];
    [self addSubview:_sortTable];
    
     return self;
}

-(void)layoutSubviews{
    
    self.sortTable.frame = CGRectMake(0,
                                      0,
                                      self.bounds.size.width,
                                      self.bounds.size.height);
}

@end

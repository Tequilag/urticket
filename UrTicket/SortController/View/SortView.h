//
//  SortView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 10.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortView : UIView

@property (nonatomic, strong) UITableView *sortTable;

@end

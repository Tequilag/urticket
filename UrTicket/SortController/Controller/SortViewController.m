//
//  SortViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 10.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "SortViewController.h"
#import "SortView.h"

@interface SortViewController ()<UITableViewDelegate, UITableViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) SortView *sortView;
@property (nonatomic, strong) UITapGestureRecognizer *gesture;

@end

NSArray *sortArray;

@implementation SortViewController

-(void)loadView{
    [super loadView];
    self.sortView = [[SortView alloc] init];
    self.view = self.sortView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    sortArray = @[@"Время", @"Цена", @"Пересадки"];
    
    self.sortView.sortTable.delegate =self;
    self.sortView.sortTable.dataSource = self;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.gesture = [[UITapGestureRecognizer alloc] init];
    self.gesture.numberOfTapsRequired = 1;
    self.gesture.numberOfTouchesRequired = 1;
    self.gesture.delegate = self;
    [self.gesture setCancelsTouchesInView:NO];
    [self.view.window addGestureRecognizer:self.gesture];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:nil];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication]
                             delegate] window];
    CGPoint pointInSubview = [self.view convertPoint:point
                                            fromView:mainWindow];
    if (!CGRectContainsPoint(self.view.frame, pointInSubview)) {
        self.gesture.delegate = nil;
        [self.view.window removeGestureRecognizer:self.gesture];
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sortArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MYCell"];
    }
    cell.textLabel.text = [sortArray objectAtIndex:indexPath.row];
    if ([sortArray objectAtIndex:indexPath.row] == self.sortValue)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.sortValue = cell.textLabel.text;
    [self.sortView.sortTable reloadData];
    //if (cell.accessoryType != UITableViewCellAccessoryCheckmark)
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

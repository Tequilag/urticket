//
//  AirportModel.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "AirportModel.h"
#import "CoreData/CoreData.h"
#import "Cities+CoreDataClass.h"
#import "Airports+CoreDataClass.h"
#import "FlightStatService.h"

#import "AppDelegate.h"

@interface AirportModel ()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;

@end

@implementation AirportModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (void)getAirportsByCityCode:(NSString *)cityCode {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityCode == %@",cityCode];
    request.predicate = predicate;
    
    self.airports = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    NSLog(@"QQ");
}

- (NSString *)getCityCodeByCityName:(NSString *)cityName {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",cityName];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *cityObj = nil;
    if ([result count] > 0) cityObj = [result firstObject];
    return cityObj.code;
}

- (NSString *)getCityNameByCityCode:(NSString *)cityCode {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",cityCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *cityObj = nil;
    if ([result count] > 0) cityObj = [result firstObject];
    return cityObj.name;
}

- (void)getNearAirportsByCoordinateWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)())completion {
    [[FlightStatService sharedService] getAirportWithinRadius:50 withLatitude:latitude andLongitude:longitude withComplition:^(NSArray * result){
        self.airports = result;
        if (completion) {
            completion();
        }
    }];
}

- (void)getCoordinateByCityName:(NSString *)cityName {
    
}

- (NSString *) getDistanceWithOriginCoordinates:(CLLocationCoordinate2D)origin andDestinationCoordinate:(CLLocationCoordinate2D)destination {
    double longitudeOrigin = origin.longitude*M_PI/180;
    double latitudeOrigin = origin.latitude*M_PI/180;
    double longitudeDestination = destination.longitude*M_PI/180;
    double latitudeDestination = destination.latitude*M_PI/180;
    double distance = acos (sin(latitudeOrigin)*sin(latitudeDestination)+
                            cos(latitudeOrigin)*cos(latitudeDestination)*cos(longitudeOrigin-longitudeDestination))*6371;
    
    return [NSString stringWithFormat:@"∽%ldкм",lroundf(distance)];
}

- (NSString *)getCityCodeByCityNameInAirports:(NSString *)cityName {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",cityName];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *airportObj = nil;
    if ([result count] > 0) airportObj = [result firstObject];
    return airportObj.code;
}

- (NSString *)getCityCodeByAiportCode:(NSString *)airportCode{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",airportCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Airports *airportObj = nil;
    if ([result count] > 0) airportObj = [result firstObject];
    return airportObj.cityCode;
}
@end


//
//  AirportModel.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AirportModel : NSObject

@property (nonatomic,strong) NSArray *airports;

- (void)getAirportsByCityCode:(NSString *)cityCode;
- (void)getNearAirportsByCoordinateWithLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)())completion;
- (NSString *)getCityCodeByCityName:(NSString *)cityName;
- (void)getCoordinateByCityName:(NSString *)cityName;
- (NSString *)getCityCodeByCityNameInAirports:(NSString *)cityName;
- (NSString *)getCityCodeByAiportCode:(NSString *)airportCode;
- (NSString *)getCityNameByCityCode:(NSString *)cityCode;
- (NSString *) getDistanceWithOriginCoordinates:(CLLocationCoordinate2D)origin andDestinationCoordinate:(CLLocationCoordinate2D)destination;

@end

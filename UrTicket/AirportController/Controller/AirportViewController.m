//
//  AirportViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "AirportViewController.h"
#import "AirportView.h"
#import "AirportModel.h"
#import "Airports+CoreDataClass.h"

@interface AirportViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate>

@property (nonatomic,strong) AirportView *airportsView;
@property (nonatomic,strong) AirportModel *airportsModel;
@property (nonatomic,strong) CLLocationManager *locationManager;

@end

@implementation AirportViewController

- (void)loadView {
    self.airportsView = [[AirportView alloc] init];
    self.view = self.airportsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.airportsModel = [[AirportModel alloc] init];
    
    if (self.localtionSearch) {
        self.cityCodeForSearch = [self.airportsModel getCityCodeByCityNameInAirports:self.cityCodeForSearch];
        self.cityName = [self.airportsModel getCityNameByCityCode:self.cityCodeForSearch];
    }

 

    
    [self.airportsModel getAirportsByCityCode:self.cityCodeForSearch];
    self.navigationItem.title = @"Выберите аэропорт";
    
    
    if ([self.airportsModel.airports count] == 0) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"В выбранном городе нет аэропортов!\nБудут показаны ближайшие"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ОК"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        [self.airportsView.indicatorView startAnimating];
        

        [self.airportsModel getNearAirportsByCoordinateWithLatitude:self.latitude andLongitude:self.longitude withComplition:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.airportsView.indicatorView stopAnimating];
            [self.airportsView.airportsTable reloadData];
            });
        }];

    }
    
    self.airportsView.airportsTable.dataSource = self;
    self.airportsView.airportsTable.delegate = self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.locationManager stopUpdatingLocation];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.airportsModel.airports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MYCell"];
    }
    Airports *airport = [self.airportsModel.airports objectAtIndex:indexPath.row];
    cell.textLabel.text = airport.name;
    if (self.localtionSearch) {
        CLLocationCoordinate2D origin = CLLocationCoordinate2DMake([self.latitude doubleValue], [self.longitude doubleValue]);
        CLLocationCoordinate2D destination = CLLocationCoordinate2DMake(airport.latitude, airport.longitude);
        cell.detailTextLabel.text = [self.airportsModel getDistanceWithOriginCoordinates:origin andDestinationCoordinate:destination];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate)
    {
        Airports *airport = [self.airportsModel.airports objectAtIndex:indexPath.row];
        [self.delegate setAirport:[self.airportsModel getCityNameByCityCode:[self.airportsModel getCityCodeByAiportCode:airport.code] ] andCode:airport.code];
        //[self.navigationController popToRootViewControllerAnimated:YES];//airport.name
        [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
    }
}

@end

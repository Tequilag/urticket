//
//  AirportViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  AirportViewControllerDelegate;

@interface AirportViewController : UIViewController

@property (nonatomic, strong) NSString *cityCodeForSearch;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, assign) BOOL localtionSearch;
@property (nonatomic, weak) id<AirportViewControllerDelegate> delegate;

@end

@protocol AirportViewControllerDelegate <NSObject>

- (void)setAirport:(NSString *)airport andCode:(NSString *)code;

@end

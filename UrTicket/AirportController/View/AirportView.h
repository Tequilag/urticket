//
//  AirportView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 26.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirportView : UIView

@property (nonatomic, strong) UITableView *airportsTable;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, assign) CGFloat topOffset;

@end

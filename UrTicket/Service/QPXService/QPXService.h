//
//  QPXService.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QPXService : NSObject

+ (instancetype)sharedService;

- (void)getResult:(NSDictionary *)options withComplition:(void (^)(NSArray *))completion;

@end

//
//  QPXService.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "QPXService.h"
#import "Mapper.h"

static NSString const *qpxAPIKey = @"AIzaSyCawtrD6NF1IdpN5dm1ne6Bm5RgOnprqdE";//@"AIzaSyDbg8Rc_VlXAxXXORhUZdSLHaFW6NCS7ck";

@interface QPXService ()

@end

@implementation QPXService

+ (instancetype)sharedService {
    static QPXService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[QPXService alloc] init];
    });
    return sharedService;
}

- (void)getResult:(NSDictionary *)options withComplition:(void (^)(NSArray * result))completion {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url1 = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyD1UDJSju9F1ixT2IjJMizscdzcbWO3EXo"]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyCawtrD6NF1IdpN5dm1ne6Bm5RgOnprqdE"]];

    NSData *postData = [NSJSONSerialization dataWithJSONObject:options options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setHTTPMethod:@"POST"];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (completion) {
            completion([Mapper mapResponseJSONToArray:data]);
        }
    }]resume];
}

@end

//
//  FlightStatService.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "FlightStatService.h"
#import "Mapper.h"

@implementation FlightStatService

+ (instancetype)sharedService {
    static FlightStatService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[FlightStatService alloc] init];
    });
    return sharedService;
}

- (void)getAirportWithinRadius:(NSInteger)radius withLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)(NSArray * result))completion {
    

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.flightstats.com/flex/airports/rest/v1/json/withinRadius/%@/%@/%li?appId=516487d8&appKey=05f6e4fef88e04490bbf51784858ac29",
                                       longitude,
                                       latitude,
                                       (long)radius]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sess = [NSURLSession sessionWithConfiguration:configuration];
    
    [[sess dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSData *rqe = data;
             NSArray * result = [Mapper mapAirportsToArrayFromData:data];
             if (completion) {
                 completion(result);
             }
         }] resume];
    
}

@end

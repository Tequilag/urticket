//
//  FlightStatService.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightStatService : NSObject

+ (instancetype)sharedService;

- (void)getAirportWithinRadius:(NSInteger)radius withLatitude:(NSNumber *)latitude andLongitude:(NSNumber *)longitude withComplition:(void (^)(NSArray * result))completion;

@end

//
//  MainViewModel.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Favorite+CoreDataClass.h"

@interface MainViewModel : NSObject

-(NSArray *)getFavorites:(int)parameter;
- (NSString *)getCityNameByCityCode:(NSString *)cityCode;
- (NSString *)getCityCodeByAirportCode:(NSString *)airportCode;
-(void)deleteFavorite:(Favorite *)item;
@end

//
//  MainViewModel.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainViewModel.h"
#import "CoreData/CoreData.h"
#import "Cities+CoreDataClass.h"
//#import "PairAirports+CoreDataClass.h"
#import "Airports+CoreDataClass.h"
#import "AppDelegate.h"

@interface MainViewModel()

@property (nonatomic,strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end


@implementation MainViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

-(NSArray *)getFavorites:(int)parameter{
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSFetchRequest *request;
    switch (parameter) {
        case 1:{
            request = [NSFetchRequest fetchRequestWithEntityName:@"Favorite"];
        }
            break;
        case 2:{
            request = [NSFetchRequest fetchRequestWithEntityName:@"RecentlyViewed"];
        }
            break;
        case 3:{
            request = [NSFetchRequest fetchRequestWithEntityName:@"RepeatedSearch"];
        }
            break;
    }
//    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"1=1"];
//    request.predicate = predicate;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateOfSave" ascending:NO];
    [request setSortDescriptors:@[sort]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                     initWithFetchRequest:request
                                     managedObjectContext:context
                                     sectionNameKeyPath:nil
                                     cacheName:nil];
    [self.fetchedResultsController performFetch:nil];//[self.persistentContainer.viewContext executeFetchRequest:request error:nil];//
    return self.fetchedResultsController.fetchedObjects;
}

- (NSString *)getCityNameByCityCode:(NSString *)cityCode {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Cities"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",cityCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Cities *cityObj = nil;
    if ([result count] > 0) cityObj = [result lastObject];
    return cityObj.name;
}

- (NSString *)getCityCodeByAirportCode:(NSString *)airportCode {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airports"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %@",airportCode];
    request.predicate = predicate;
    [request setFetchLimit:1];
    
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    
    Airports *airportObj = nil;
    if ([result count] > 0) airportObj = [result firstObject];
    return airportObj.cityCode;
}

-(void)deleteFavorite:(Favorite *)item{
//    NSArray *pairAirports = [item.pairAirports array];
//    for (PairAirports *pair in pairAirports) {
//        [self.persistentContainer.viewContext deleteObject:pair];
//    }
    [self.persistentContainer.viewContext deleteObject:item];
    [self.persistentContainer.viewContext save:nil];
}
@end

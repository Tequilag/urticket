//
//  MainViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 14.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketViewModel.h"



@interface MainViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UIBarButtonItem *searchAirportButton;
@property (nonatomic, strong) UIBarButtonItem *searchHotelButton;
@property (nonatomic, strong) UIBarButtonItem *searchCarsButton;

@property (nonatomic, strong) TicketViewModel *ticketViewModel;

@end

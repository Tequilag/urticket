//
//  MainViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 14.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainViewController.h"
#import "MainView.h"
#import "SearchViewController.h"
#import "CoreData/CoreData.h"
#import "CalendarViewController.h"
#import "MainFavoriteTableViewCellContent.h"
#import "MainRepeatedTableViewCellContent.h"
#import "MainViewModel.h"
#import "FilterViewModel.h"
#import "TicketViewController.h"
#import "TicketView.h"
#import "MainTicketViewController.h"
#import "DetailViewController.h"

@interface MainViewController ()<SearchViewControllerDelegate, TicketViewControllerDelegate, UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) MainView* mainView;
@property (nonatomic, strong) NSArray *favoriteArray;
@property (nonatomic, strong) NSArray *recentlyViewedArray;
@property (nonatomic, strong) NSArray *repeatedSearchArray;
@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@property (nonatomic, strong) MainViewModel *viewModel;
@property (nonatomic, strong) FilterViewModel *filterviewModel;

@end

CGFloat widthM;
CGFloat heightM;

@implementation MainViewController

- (void)loadView {
    [super loadView];
    self.mainView = [[MainView alloc]init];
    self.viewModel = [[MainViewModel alloc] init];
    self.view = self.mainView;
    
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Главный экран";
    [self.mainView.searchAirportButton addTarget:self action:@selector(addOptions:) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.searchCarButton addTarget:self action:@selector(captureScreen) forControlEvents:UIControlEventTouchUpInside];
    self.favoriteArray = [NSArray arrayWithArray:[self.viewModel getFavorites:1]];
    self.recentlyViewedArray = [NSArray arrayWithArray:[self.viewModel getFavorites:2]];
    self.repeatedSearchArray = [NSArray arrayWithArray:[self.viewModel getFavorites:3]];
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    if (!self.filterviewModel)
    self.filterviewModel = [[FilterViewModel alloc ] init];
    UIBarButtonItem *btn_Back = [[UIBarButtonItem alloc] initWithTitle:@"Назад"
                                                                 style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem=btn_Back;
    self.mainView.colView.delegate =self;
    self.mainView.colView.dataSource =self;
    
    self.mainView.recentlyViewed.delegate =self;
    self.mainView.recentlyViewed.dataSource =self;
    
    self.mainView.repeatedSearch.delegate =self;
    self.mainView.repeatedSearch.dataSource =self;

}

- (UIImage *) captureScreen {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = CGRectMake(keyWindow.frame.origin.x,
                             keyWindow.frame.origin.y,
                             keyWindow.frame.size.width,
                             keyWindow.frame.size.height/2);//[keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.mainView.imageView.image = img;
    return img;
}
-(void) viewDidAppear:(BOOL)animated{
    widthM = self.view.bounds.size.width;
    heightM = self.view.bounds.size.height;
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//    layout.sectionInset = UIEdgeInsetsMake(0,
//                                           8,
//                                           0,
//                                           8);
//    layout.itemSize = CGSizeMake(widthM - 16,  heightM);
//    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    self.mainView.colView.collectionViewLayout = layout;
//    self.mainView.recentlyViewed.collectionViewLayout = layout;
//    self.mainView.repeatedSearch.collectionViewLayout = layout;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addOptions:(UIButton *)sender {
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    searchViewController.filterViewModel = self.filterviewModel;
    searchViewController.delegate = self;
    [self.navigationController pushViewController:searchViewController animated:YES];
    [self.view endEditing:YES];
}

-(void)dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)reloadTables:(UIImage *)image{
    self.favoriteArray = [NSArray arrayWithArray:[self.viewModel getFavorites:1]];
    self.recentlyViewedArray = [NSArray arrayWithArray:[self.viewModel getFavorites:2]];
    self.repeatedSearchArray = [NSArray arrayWithArray:[self.viewModel getFavorites:3]];
    [self.mainView.colView reloadData];
    [self.mainView.recentlyViewed reloadData];
    [self.mainView.repeatedSearch reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.textColor =[UIColor blackColor];
    infoLabel.font = [UIFont systemFontOfSize:14];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.text = @"Нет данных";
    switch (collectionView.tag) {
        case 1:{
            if ([self.favoriteArray count] == 0)
                    collectionView.backgroundView = infoLabel;
            
            return [self.favoriteArray count];
        }
            break;
            
        case 2:{
            if ([self.recentlyViewedArray count] == 0)
                collectionView.backgroundView = infoLabel;
            
            return [self.recentlyViewedArray count];
        }
            break;
            
        case 3:{
            if ([self.repeatedSearchArray count] == 0)
                collectionView.backgroundView = infoLabel;
            
            return [self.repeatedSearchArray count];
        }
            break;
    }


    return 0;//[self.favoriteArray count];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (collectionView.tag) {
        case 1:{
            DetailViewController *detailViewController = [[DetailViewController alloc]init];
            detailViewController.resultItem = [NSKeyedUnarchiver unarchiveObjectWithData:[[self.favoriteArray objectAtIndex:indexPath.row] valueForKey:@"savedArray"]];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
            break;
            
        case 2:{
            DetailViewController *detailViewController = [[DetailViewController alloc]init];
            detailViewController.resultItem = [NSKeyedUnarchiver unarchiveObjectWithData:[[self.recentlyViewedArray objectAtIndex:indexPath.row] valueForKey:@"savedArray"]];//;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
            break;
    }

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    switch (collectionView.tag) {
        case 1:{
            MainFavoriteTableViewCellContent *cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];

            if ([self.favoriteArray count] >0 ){
                [cell1 setFavorite:[self.favoriteArray objectAtIndex:indexPath.row]];
                [cell1.addToLiked addTarget:self action:@selector(deleteItem:) forControlEvents:UIControlEventTouchUpInside];
                [cell1.addToLiked setImage:[UIImage imageNamed:@"addedStar"] forState:UIControlStateNormal];
                cell1.addToLiked.tag = indexPath.row;
            }
            //else [cell1 showInfo];
            return cell1;
        }
            break;
            
        case 2:{
             MainFavoriteTableViewCellContent *cell2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier2" forIndexPath:indexPath];
            if ([self.recentlyViewedArray count] >0 ){
                [cell2 setRecentlyViewed:[self.recentlyViewedArray objectAtIndex:indexPath.row]];
                
            }
            //else [cell2 showInfo];
            return cell2;
        }
            break;
            
        case 3:{
            //UIImageView *tempVeiew = [[UIImageView alloc] initWithImage:[self.repeatedSearchArray objectAtIndex:indexPath.row]];
            MainRepeatedTableViewCellContent *cell3 = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier3" forIndexPath:indexPath];
            if ([self.repeatedSearchArray count] >0 ){
                [cell3 setRepeatedSearch:[self.repeatedSearchArray objectAtIndex:indexPath.row]];//[self.repeatedSearchArray count] - indexPath.row - 1]];
                [cell3.countOfTickets addTarget:self action:@selector(openTicketViewController:) forControlEvents:UIControlEventTouchUpInside];
                [cell3.search addTarget:self action:@selector(startSearching:) forControlEvents:UIControlEventTouchUpInside];
                cell3.countOfTickets.tag = indexPath.row;
                cell3.search.tag = indexPath.row;
            }
            
            return cell3;
        }
            break;
    }
    return cell;
}

-(void)deleteItem:(UIButton *)sender{
    MainViewModel *mainViewModel = [[MainViewModel alloc] init];
    [mainViewModel deleteFavorite:[self.favoriteArray objectAtIndex:sender.tag]];
    self.favoriteArray = [NSArray arrayWithArray:[self.viewModel getFavorites:1]];
    [self.mainView.colView reloadData];
}

-(void)openTicketViewController:(UIButton *)sender{
    MainTicketViewController *ticketViewController = [[MainTicketViewController alloc ] init];
    ticketViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    RepeatedSearch *temp  = [self.repeatedSearchArray objectAtIndex:sender.tag];//[self.repeatedSearchArray count] - sender.tag - 1];
    NSDictionary *tempDictionary = [NSKeyedUnarchiver unarchiveObjectWithData: [temp valueForKey:@"savedArray"]];
    TicketViewModel *tempModel = [[TicketViewModel alloc] init];
    tempModel.countOfAdults = [tempDictionary valueForKey:@"countOfAdults"];
    tempModel.countOfTeens = [tempDictionary valueForKey:@"countOfTeens"];
    tempModel.countOfChildren = [tempDictionary valueForKey:@"countOfChildren"];
    tempModel.prefferedCabin = [tempDictionary valueForKey:@"prefferedCabin"];
    tempModel.airline = [tempDictionary valueForKey:@"airline"];
    tempModel.from = [tempDictionary valueForKey:@"from"];
    tempModel.to = [tempDictionary valueForKey:@"to"];
    tempModel.dateOfRequest = [tempDictionary valueForKey:@"dateOfRequest"];
    tempModel.dateOfDeparture = [tempDictionary valueForKey:@"dateOfDeparture"];
    ticketViewController.ticketViewModel = tempModel;
//sender.tag];//
    [self presentViewController:ticketViewController animated:YES completion:nil];
}

-(void)startSearching:(UIButton *)sender{
    SearchViewController *searchViewController = [[SearchViewController alloc] init];
    RepeatedSearch *temp  = [self.repeatedSearchArray objectAtIndex:sender.tag];//[self.repeatedSearchArray count] - sender.tag - 1];
    NSDictionary *tempDictionary = [NSKeyedUnarchiver unarchiveObjectWithData: [temp valueForKey:@"savedArray"]];
    TicketViewModel *tempModel = [[TicketViewModel alloc] init];
    tempModel.countOfAdults = [tempDictionary valueForKey:@"countOfAdults"];
    tempModel.countOfTeens = [tempDictionary valueForKey:@"countOfTeens"];
    tempModel.countOfChildren = [tempDictionary valueForKey:@"countOfChildren"];
    tempModel.prefferedCabin = [tempDictionary valueForKey:@"prefferedCabin"];
    tempModel.airline = [tempDictionary valueForKey:@"airline"];
    tempModel.from = [tempDictionary valueForKey:@"from"];
    tempModel.to = [tempDictionary valueForKey:@"to"];
    tempModel.dateOfRequest = [tempDictionary valueForKey:@"dateOfRequest"];
    //tempModel.dateOfDeparture = [tempDictionary valueForKey:@"dateOfDeparture"];
    searchViewController.ticketViewModel = tempModel;
    searchViewController.filterViewModel = self.filterviewModel;
    searchViewController.delegate = self;
    [self.navigationController pushViewController:searchViewController animated:YES];
    [self.view endEditing:YES];
}


-(void)setTicket:(TicketViewModel *)value{
    self.ticketViewModel = value;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)-4));
}
@end

//
//  MainRepeatedTableViewCellContent.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainRepeatedTableViewCellContent.h"
#import "MainViewModel.h"

@implementation MainRepeatedTableViewCellContent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0];
    self.layer.cornerRadius = 15.0f;
    
    UIColor *buttonColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1];
    
    _cityFrom = [[UILabel alloc] init];
    _cityFrom.tag = 1;
    [_cityFrom setFont:[UIFont boldSystemFontOfSize:13]];
    _cityFrom.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_cityFrom];
    
    _cityTo = [[UILabel alloc] init];
    _cityTo.tag = 2;
    [_cityTo setFont:[UIFont boldSystemFontOfSize:13]];
    _cityTo.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_cityTo];
    
    
    _dateDeparture = [[UILabel alloc] init];
    _dateDeparture.tag = 3;
    [_dateDeparture setFont:[UIFont boldSystemFontOfSize:13]];
   //_dateDeparture.layer.backgroundColor = [UIColor grayColor].CGColor;
    _dateDeparture.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_dateDeparture];
    
    _countOfTickets = [[UIButton alloc] init];
    _countOfTickets.tag = 1;
    _countOfTickets.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //[_countOfTickets setAlpha:1];
    _countOfTickets.layer.borderWidth = 1.0f;
    _countOfTickets.layer.borderColor  = buttonColor.CGColor;
    _countOfTickets.layer.cornerRadius = 5.0f;
    [_countOfTickets setTitleColor:buttonColor forState:UIControlStateNormal];
    [_countOfTickets.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_countOfTickets.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_countOfTickets setTitle:@"Билет" forState:UIControlStateNormal];
    [self addSubview:_countOfTickets];
    
    _search = [[UIButton alloc] init];
    _search.tag = 2;
    _search.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _search.layer.borderWidth = 1.0f;
    _search.layer.borderColor  = buttonColor.CGColor;
    _search.layer.cornerRadius = 5.0f;
    [_search setTitleColor:buttonColor forState:UIControlStateNormal];
    [_search.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_search.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [_search setTitle:@"Поиск" forState:UIControlStateNormal];
    [self addSubview:_search];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width  = self.bounds.size.width;
    CGFloat height  = self.bounds.size.height;
    CGFloat offset = width * 0.1f;
    //CGFloat buttonOffset = 1;
    CGFloat textFieldHeight = height*0.07f;
    CGFloat textFieldWidth = width*0.8f;
    CGFloat buttonWidth = (width - offset*2)/2 - offset;
    
    self.cityFrom.frame = CGRectMake(offset,
                                     offset,
                                     textFieldWidth,
                                     textFieldHeight);
    
    self.cityTo.frame = CGRectMake(offset,
                                   self.cityFrom.frame.origin.y + textFieldHeight + offset,
                                   textFieldWidth,
                                   textFieldHeight);
    
    self.dateDeparture.frame = CGRectMake(offset,
                                          self.cityTo.frame.origin.y + textFieldHeight + offset,
                                          textFieldWidth,//*0.62f,
                                          textFieldHeight);

  //  self.countOfTickets.frame = CGRectMake(self.dateDeparture.frame.origin.x + self.dateDeparture.frame.size.width + (((width - (self.dateDeparture.frame.origin.x + self.dateDeparture.frame.size.width))/2 - buttonWidth/2)),// + width * 0.01f,
//                                           self.dateDeparture.frame.origin.y,// + textFieldHeight + width*0.01f,
//                                           buttonWidth,
//                                           textFieldHeight);
    self.countOfTickets.frame = CGRectMake(offset,
                                           self.dateDeparture.frame.origin.y + textFieldHeight + width*0.05f,
                                           buttonWidth,
                                           height*0.1f);
    self.search.frame = CGRectMake(width - offset - buttonWidth,
                                   self.countOfTickets.frame.origin.y,// + self.countOfTickets.frame.size.height + width * 0.05f,
                                   buttonWidth,
                                   height*0.1f);
    //self.search.layer.cornerRadius = offset;
    //self.countOfTickets.layer.cornerRadius = offset;
//    buttonOffset = ((width - offset*2.0f) - buttonWidth*0.4f - buttonWidth*0.3f - buttonWidth*0.67f)/2.0f;
//    
//    self.countOfTickets.frame = CGRectMake(offset,
//                                           self.dateDeparture.frame.origin.y + self.dateDeparture.frame.size.height,
//                                           buttonWidth*0.3f,
//                                           textFieldHeight);
}

- (void)setRepeatedSearch:(id)obj2 {
    NSDateFormatter *dateFormatF = [[NSDateFormatter alloc] init];
    [dateFormatF setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *dateFormatT = [[NSDateFormatter alloc] init];
    [dateFormatT setDateFormat:@"dd.MM.yy HH:mm:ss a"];
    [dateFormatT setTimeZone:[NSTimeZone systemTimeZone]];
    
    //[dateFormatT setTimeStyle:NSDateFormatterLongStyle];
    RepeatedSearch *obj= obj2;
    MainViewModel *mainViewModel= [[MainViewModel alloc] init];
    NSArray *segment =[NSKeyedUnarchiver unarchiveObjectWithData:[obj valueForKey:@"savedArray"]];
    //NSArray *segment = [item objectForKey:@"segment"];
    
    self.cityFrom.text = [NSString stringWithFormat:@"Вылет: %@ (%@)",[mainViewModel getCityNameByCityCode:[mainViewModel
                                                                              getCityCodeByAirportCode:[segment valueForKey:@"from"]]],[segment valueForKey:@"from"]];
    
    self.cityTo.text = [NSString stringWithFormat:@"Прилет: %@ (%@)",[mainViewModel getCityNameByCityCode:[mainViewModel
                                                                            getCityCodeByAirportCode:[segment  valueForKey:@"to"]]],[segment valueForKey:@"to"]];
    //NSDate *tempDate =;
    NSString *tempStrDate = [dateFormatT stringFromDate: [segment valueForKey:@"dateOfRequest"]];
    //NSArray *timeDep = [[NSString stringWithFormat:@"%@",[segment  valueForKey:@"dateOfRequest"]] componentsSeparatedByString:@" "];
    NSArray *timeDep = [tempStrDate componentsSeparatedByString:@" "];
//    self.dateDeparture.text = [NSString stringWithFormat:@"Дата запроса: %@ в %@",[dateFormatT stringFromDate:[dateFormatF dateFromString:timeDep[0]]], timeDep[1]];
    self.dateDeparture.text = [NSString stringWithFormat:@"Дата запроса: %@ в %@",timeDep[0], timeDep[1]];
                               //[dateFormatT stringFromDate:[dateFormatF dateFromString:timeDep[0]]];
                               //[timeDep[1] componentsSeparatedByString:@"+"][0]];
    [self setNeedsLayout];
}

@end

//
//  MainView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 14.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainView : UIView

@property (nonatomic, assign) CGFloat topOffset;
@property (nonatomic, strong) UIView *modalView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIButton *searchAirportButton;
@property (nonatomic, strong) UIButton *searchHotelButton;
@property (nonatomic, strong) UIButton *searchCarButton;

@property (nonatomic, strong) UICollectionView *colView;
@property (nonatomic, strong) UILabel *colViewLabel;

@property (nonatomic, strong) UICollectionView *recentlyViewed;
@property (nonatomic, strong) UILabel *recentlyViewedLabel;

@property (nonatomic, strong) UICollectionView *repeatedSearch;
@property (nonatomic, strong) UILabel *repeatedSearchLabel;

@property (nonatomic, strong) UIImageView *imageView;

@end


//
//  MainFavoriteTableViewCellContent.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 19.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Favorite+CoreDataClass.h"
#import "RecentlyViewed+CoreDataClass.h"

@interface MainFavoriteTableViewCellContent : UICollectionViewCell


@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *timeFromLabel;
@property (nonatomic, strong) UILabel *timeToLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *stopsLabel;
@property (nonatomic, strong) UILabel *countStops;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UILabel *destinationLabel;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, strong) UIView *noData;
@property (nonatomic, strong) UILabel *infoLabel;

@property (nonatomic, strong) UIButton *addToLiked;

- (void)setFavorite:(Favorite *)item;
-(void)setRecentlyViewed:(RecentlyViewed *)item;

- (void)prepareForReuse;

@end

//
//  MainView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 14.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MainView.h"
#import "MainFavoriteTableViewCellContent.h"
#import "MainRepeatedTableViewCellContent.h"

static CGFloat const ViewContentMargin = 8.0f;

@implementation MainView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.scrollEnabled = YES;
    [_scrollView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:_scrollView];
    
    _searchAirportButton = [[UIButton alloc] init];
    _searchAirportButton.tag = 3;
    _searchAirportButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
    [_searchAirportButton setTitleColor:[UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1] forState:UIControlStateNormal];
    [_searchAirportButton.titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
    [_searchAirportButton.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [_searchAirportButton setTitle:@"Начать поиск" forState:UIControlStateNormal];
    _searchAirportButton.layer.borderColor = _searchAirportButton.currentTitleColor.CGColor;
    _searchAirportButton.layer.borderWidth = 1.0f;
    _searchAirportButton.layer.cornerRadius = 15.0f;
    [self.scrollView addSubview:_searchAirportButton];
    
    _searchCarButton = [[UIButton alloc] init];
    _searchCarButton.layer.cornerRadius = 23.0f;
    _searchCarButton.layer.borderColor=[UIColor grayColor].CGColor;
    _searchCarButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _searchCarButton.layer.borderWidth=1.0f;

    
    _searchHotelButton = [[UIButton alloc] init];
    _searchHotelButton.layer.cornerRadius = 23.0f;
    _searchHotelButton.layer.borderColor=[UIColor grayColor].CGColor;
    _searchHotelButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _searchHotelButton.layer.borderWidth=1.0f;
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(ViewContentMargin/2,
                                           0,
                                           ViewContentMargin/2,
                                           0);
    layout.itemSize = CGSizeMake(10,  10);
    layout.minimumLineSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _colView.layer.backgroundColor = [UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0].CGColor;
    _colView.tag=1;
    _colView.showsHorizontalScrollIndicator = NO;
    _colView.pagingEnabled = YES;
    [_colView registerClass:[MainFavoriteTableViewCellContent class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.scrollView addSubview:_colView];
    
    _colViewLabel = [[UILabel alloc] init];
    _colViewLabel.text = @"Избранное";
    _colViewLabel.textColor =[UIColor blackColor];
    _colViewLabel.textAlignment = NSTextAlignmentCenter;
    _colViewLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:_colViewLabel];
    
    UICollectionViewFlowLayout *layout2 = [[UICollectionViewFlowLayout alloc] init];
    layout2.sectionInset = UIEdgeInsetsMake(ViewContentMargin/2,
                                           0,
                                           ViewContentMargin/2,
                                           0);
    layout2.itemSize = CGSizeMake(10,  10);
    layout2.minimumLineSpacing = 0;
    layout2.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _recentlyViewed = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout2];
    _recentlyViewed.layer.backgroundColor = [UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0].CGColor;
    _recentlyViewed.tag =2;
    _recentlyViewed.showsHorizontalScrollIndicator = NO;
    _recentlyViewed.pagingEnabled = YES;
    [_recentlyViewed registerClass:[MainFavoriteTableViewCellContent class] forCellWithReuseIdentifier:@"cellIdentifier2"];
    [self.scrollView addSubview:_recentlyViewed];
    
    _recentlyViewedLabel = [[UILabel alloc] init];
    _recentlyViewedLabel.text = @"Недавно просмотренные";
    _recentlyViewedLabel.textColor =[UIColor blackColor];
    _recentlyViewedLabel.textAlignment = NSTextAlignmentCenter;
    _recentlyViewedLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:_recentlyViewedLabel];

    
    UICollectionViewFlowLayout *layout3 = [[UICollectionViewFlowLayout alloc] init];
    layout3.sectionInset = UIEdgeInsetsMake(ViewContentMargin/2,
                                            0,
                                            ViewContentMargin/2,
                                            0);
    layout3.itemSize = CGSizeMake(10,  10);
    layout3.minimumLineSpacing = 0;
    layout3.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    _repeatedSearch = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout3];
    _repeatedSearch.layer.backgroundColor = [UIColor colorWithRed:0.9 green:0.89 blue:0.89 alpha:1.0].CGColor;
    _repeatedSearch.pagingEnabled=YES;
    _repeatedSearch.tag=3;
    _repeatedSearch.showsHorizontalScrollIndicator = NO;
    [_repeatedSearch registerClass:[MainRepeatedTableViewCellContent class] forCellWithReuseIdentifier:@"cellIdentifier3"];
    [self.scrollView addSubview:_repeatedSearch];
    
    _repeatedSearchLabel = [[UILabel alloc] init];
    _repeatedSearchLabel.text = @"Повторный поиск";
    _repeatedSearchLabel.textColor =[UIColor blackColor];
    _repeatedSearchLabel.textAlignment = NSTextAlignmentCenter;
    _repeatedSearchLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:_repeatedSearchLabel];
    
    _imageView = [[UIImageView alloc] init];
    [self.scrollView addSubview:_imageView];
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];

    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat searchButtonWidth = width * 0.3125f;
    CGFloat searchButtonHeight = 46.0f;
    CGFloat offset = width * 0.05;
    CGFloat heightLabel = height * 0.04;
    
    self.scrollView.frame = CGRectMake(0,
                                       0,
                                       width,
                                       height);
    self.searchAirportButton.frame = CGRectMake(width/2 - searchButtonWidth/2,
                                            height*0.05f,
                                            searchButtonWidth,
                                            searchButtonHeight);
    
    self.colViewLabel.frame = CGRectMake(offset,
                                         self.searchAirportButton.frame.origin.y + self.searchAirportButton.frame.size.height + offset,
                                         width-offset*2,
                                         heightLabel);
    
    self.colView.frame = CGRectMake(0,
                                   self.colViewLabel.frame.origin.y + self.colViewLabel.frame.size.height,
                                   width,
                                   height*0.33);
  
    
    self.recentlyViewedLabel.frame = CGRectMake(offset,
                                                self.colView.frame.origin.y + self.colView.frame.size.height + offset,
                                                width-offset*2,
                                                heightLabel);
    
    self.recentlyViewed.frame = CGRectMake(0,
                                           self.recentlyViewedLabel.frame.origin.y + self.recentlyViewedLabel.frame.size.height + offset,
                                           width,
                                           height*0.33);
    
    self.repeatedSearchLabel.frame = CGRectMake(offset,
                                                self.recentlyViewed.frame.origin.y + self.recentlyViewed.frame.size.height + offset,
                                                width-offset*2,
                                                heightLabel);
    
    self.repeatedSearch.frame = CGRectMake(0,
                                    self.repeatedSearchLabel.frame.origin.y + self.repeatedSearchLabel.frame.size.height + offset,
                                    width,
                                    height*0.33);
    
    self.scrollView.contentSize = CGSizeMake(width, self.repeatedSearch.frame.origin.y + self.repeatedSearch.frame.size.height + offset);

}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end

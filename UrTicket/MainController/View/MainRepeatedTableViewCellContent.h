//
//  MainRepeatedTableViewCellContent.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 23.05.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepeatedSearch+CoreDataClass.h"

@interface MainRepeatedTableViewCellContent : UICollectionViewCell

@property (nonatomic, strong) UILabel *cityFrom;
@property (nonatomic, strong) UILabel *cityTo;
@property (nonatomic, strong) UILabel *dateDeparture;

@property (nonatomic, strong) UIButton *countOfTickets;
@property (nonatomic, strong) UIButton *search;

- (void)setRepeatedSearch:(id)item;
@end

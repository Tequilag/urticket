//
//  CalendarViewController.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 22.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "CalendarViewController.h"
#import "CalendarView.h"
#import "SearchView.h"
@interface CalendarViewController ()<UIGestureRecognizerDelegate>

@property (nonatomic,strong) CalendarView *cv;
@property (nonatomic,strong) SearchView *sv;
@property (nonatomic, strong) UITapGestureRecognizer *gesture;
@property (nonatomic) id lastDate;
@property (nonatomic) NSDateComponents* currentValue;
@property (nonatomic) NSDateComponents* currentDay;

@end

NSUInteger numDays;
int thisYear;
int weekday;
int thisMonth;

NSArray * createdAt;
NSArray * parseSpot3;
NSArray * hadSession;

CGFloat width;
CGFloat height;

@implementation CalendarViewController

-(void)loadView {
    [super loadView];
    self.cv = [[CalendarView alloc] init];
    self.view = self.cv;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentValue = [[NSDateComponents alloc] init];
    [self.cv.prev addTarget:self action:@selector(prevAct:) forControlEvents:UIControlEventTouchUpInside];
    [self.cv.next addTarget:self action:@selector(nextAct:) forControlEvents:UIControlEventTouchUpInside];
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    width = self.view.bounds.size.width;
    height = self.view.bounds.size.height;
    self.gesture = [[UITapGestureRecognizer alloc] init];
    self.gesture.numberOfTapsRequired = 1;
    self.gesture.numberOfTouchesRequired = 1;
    self.gesture.delegate = self;
    [self.gesture setCancelsTouchesInView:NO];
    [self.view.window addGestureRecognizer:self.gesture];
    [self myCalView];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:nil];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication]
                             delegate] window];
    CGPoint pointInSubview = [self.view convertPoint:point
                                            fromView:mainWindow];
    if (!CGRectContainsPoint(self.view.frame, pointInSubview)) {
        self.gesture.delegate = nil;
        [self.view.window removeGestureRecognizer:self.gesture];
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES; 
}

//Next and previous buttons
//they each call the removeTags method which removes the buttons from the view
//the buttons are added again in the updateCallNow method

- (IBAction)nextAct:(UIButton *)sender {
    thisMonth++;
    [self removeTags];
    [self updateCalNow];
}

- (IBAction)prevAct:(UIButton *)sender {
    thisMonth--;
    [self removeTags];
    [self updateCalNow];
}

-(void) removeTags{
    int x=1;
    while (x<=31){
        [[self.view viewWithTag:x] removeFromSuperview];
        x++;
    }
    
}

/*
 this method returns the number of days in the month in the date it is sent
 */

-(NSUInteger)getCurrDateInfo:(NSDate *)myDate{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange rng = [cal rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:myDate];
    NSUInteger numberOfDaysInMonth = rng.length;
    return numberOfDaysInMonth;
}

/*
 This is the method called to create the calendar
 */

-(void)myCalView{
    NSDateFormatter *dateFormatFrom = [[NSDateFormatter alloc] init];
    [dateFormatFrom setDateFormat:@"yyyy-MM-dd"];
    NSArray *strgg =[[dateFormatFrom stringFromDate:[NSDate date]] componentsSeparatedByString:@"-"];
    int currentValue =  [strgg[2] intValue];
    thisYear = [[[NSCalendar currentCalendar]
                 components:NSCalendarUnitYear fromDate:[NSDate date]]
                year];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps2 = [cal components:NSCalendarUnitMonth fromDate:[NSDate date]];
    thisMonth = [comps2 month];
    self.currentDay = [[NSDateComponents alloc] init];
    [_currentDay setDay:currentValue];
    [_currentDay setMonth:thisMonth];
    [_currentDay setYear:thisYear];
    [self moreDateInfo];
}

-(void)updateCalNow{// try to condense this so only one method is used instead of two
    if(thisMonth>12){
        thisMonth=1;
        thisYear++;
    }
    if(thisMonth<1){
        thisMonth=12;
        thisYear--;
    }
    [self moreDateInfo];
}

-(void)moreDateInfo{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateFormatter *dateFormatFrom = [[NSDateFormatter alloc] init];
    [dateFormatFrom setDateFormat:@"yyyy-MM-dd"];
    //get first day of month's weekday
   ;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:1];
    [components setMonth:thisMonth];
    [components setYear:thisYear];
    //_currentDay = components;
    NSDate * newDate = [calendar dateFromComponents:components];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:newDate];
    weekday = [comps weekday];
    //Get number of days in the month
    numDays=[self getCurrDateInfo:newDate];
    
    CGFloat offset = self.cv.tuesday.frame.origin.x - (self.cv.monday.frame.origin.x + self.cv.monday.bounds.size.width);
    CGFloat heightOffset = self.cv.monday.frame.origin.y + self.cv.monday.bounds.size.height + offset;
    CGFloat widthOffset = self.cv.monday.frame.origin.x;
    CGFloat buttonWidth = self.cv.monday.frame.size.width;
    float newWeekDay=weekday-1;

    //coordinates for displaying the buttons
    float yVal= height*0.05;
    int yCount= 0;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"RU"]];
    self.cv.month.text=[[formatter standaloneMonthSymbols] objectAtIndex:(thisMonth - 1)];
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //use for loop to display each day
    for(int startD=1; startD<=numDays;startD++){
        [components setDay:startD];
        UIButton *addProject = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        float xCoord=(newWeekDay*(buttonWidth + offset)) + widthOffset;
        float yCoord=(yCount*(yVal+offset)) + heightOffset;
        newWeekDay++;
        if(newWeekDay>6){//drops buttons on y axis every 7 days
            newWeekDay=0;
            yCount++;
        }
        //Creates the buttons and gives them each a tag (id)
        addProject.frame = CGRectMake(xCoord, yCoord, buttonWidth, yVal);
        [addProject setTitle:[NSString stringWithFormat:@"%d", startD] forState:UIControlStateNormal];
        [addProject addTarget:self action:@selector(dateDidSelected:) forControlEvents:UIControlEventTouchUpInside];
        [addProject setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        addProject.layer.cornerRadius = 5;
        addProject.layer.backgroundColor = [UIColor whiteColor].CGColor;
        if ((components.day == _currentDay.day)&&(components.month == _currentDay.month)&&(components.year == _currentDay.year)){
            addProject.layer.backgroundColor = [UIColor orangeColor].CGColor;//[UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1].CGColor;
            _lastDate = addProject;
            //[self dateDidSelected:addProject];
        }
        if ((components.day == _currentValue.day)&&(components.month == _currentValue.month)&&(components.year == _currentValue.year) && (addProject.layer.backgroundColor!= [UIColor orangeColor].CGColor)){
            addProject.layer.backgroundColor = [UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1].CGColor;
            _lastDate = addProject;
        }
        addProject.tag = startD;
        
        //if you are using a database this section checks to see if
        //a certain criteria is met. If so, you can give the button a different background color.
        BOOL match=NO;
        for(int parseNum=0; parseNum<createdAt.count; parseNum++){
            //Break Down date from Parse
            NSDate * parseDate = createdAt[parseNum];
            NSDateComponents * parseComp = [gregorian components:NSCalendarUnitMonth fromDate:parseDate];
            int parseMonth=(int)[parseComp month];
            int parseYear=(int)[[[NSCalendar currentCalendar]components:NSCalendarUnitYear fromDate:parseDate] year];
            int parseDay= (int)[[[NSCalendar currentCalendar]components:NSCalendarUnitDay fromDate:parseDate] day];
            if((parseYear==thisYear) && (parseMonth==thisMonth) && (parseDay==startD)){
                match=YES;
                if([hadSession[parseNum] isEqual:@"YES"])
                    addProject.backgroundColor = [UIColor redColor];
                else
                    addProject.backgroundColor = [UIColor greenColor];
                NSLog(@"Match %d", startD);
            }
        }
        if(match==NO)
        
        [self.cv.subView addSubview:addProject];
    }
}

-(void)dateDidSelected:(UIButton *)sender{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:[sender.currentTitle integerValue]];
    [components setMonth:thisMonth];
    [components setYear:thisYear];

    if (!(((components.day < _currentDay.day)&&((components.month <= _currentDay.month)&&(components.year <= _currentDay.year))))){
        _currentValue = components;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        UIButton *lastButton =(UIButton *) _lastDate;
        if (lastButton.layer.backgroundColor!= [UIColor orangeColor].CGColor){
            lastButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
        }
        //if (lastButton != sender)
        if (!(((components.day == _currentDay.day)&&(components.month == _currentDay.month)&&(components.year == _currentDay.year))||(sender.layer.backgroundColor == [UIColor orangeColor].CGColor)))
            sender.layer.backgroundColor =[UIColor colorWithRed:66/255.0 green:185/255.0 blue:244/255.0 alpha:1].CGColor;
        _lastDate = sender;
        NSDate * newDate = [calendar dateFromComponents:components];
        //Formats date to YYYY-MM-DD
        NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"date" object:[dateFormat stringFromDate:newDate]];
    }
    else{
        __weak typeof(self) weakSelf = self;
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Невозможно найти рейс в прошлом."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ОК"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        
        [alert addAction:yesButton];
        [weakSelf presentViewController:alert animated:YES completion:nil];
    }
}

@end

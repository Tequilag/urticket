//
//  CalendarViewController.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 22.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarViewControllerDelegate <NSObject>

-(void)disMissView;

@end

extern NSArray * createdAt;
extern NSArray * parseSpot3;
extern NSArray * hadSession;

@interface CalendarViewController : UIViewController

- (IBAction)nextAct:(id)sender;
- (IBAction)prevAct:(id)sender;

@property (nonatomic, assign) id<CalendarViewControllerDelegate> delegate;

@end

//
//  CalendarView.h
//  UrTicket
//
//  Created by Gorbenko Georgy on 22.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarView : UIView

@property (nonatomic, assign) CGFloat topOffset;

@property(nonatomic, strong) UIView *subView;

@property (nonatomic, strong) UIButton *prev;
@property (nonatomic, strong) UIButton *next;

@property (nonatomic, strong) UILabel *month;
@property (nonatomic, strong) UILabel *monday;
@property (nonatomic, strong) UILabel *tuesday;
@property (nonatomic, strong) UILabel *wednesday;
@property (nonatomic, strong) UILabel *thursday;
@property (nonatomic, strong) UILabel *friday;
@property (nonatomic, strong) UILabel *saturday;
@property (nonatomic, strong) UILabel *sunday;

@end

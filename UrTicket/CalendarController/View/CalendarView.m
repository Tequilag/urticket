//
//  CalendarView.m
//  UrTicket
//
//  Created by Gorbenko Georgy on 22.04.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "CalendarView.h"

@interface CalendarView ()

@end

@implementation CalendarView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor clearColor];
    self.opaque = NO;
    
    _subView = [[UIView alloc] init];
    _subView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_subView];
    
    _prev= [[UIButton alloc] init];
    _prev.layer.cornerRadius = 15.0f;
    //_prev.layer.borderColor=[UIColor grayColor].CGColor;
    _prev.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //_prev.layer.borderWidth=1.0f;
    [_prev setImage:[UIImage imageNamed:@"LeftArrow"] forState:UIControlStateNormal];
    [self.subView addSubview:_prev];

    _next = [[UIButton alloc] init];
    _next.layer.cornerRadius = 15.0f;
    //_next.layer.borderColor=[UIColor grayColor].CGColor;
    _next.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //_next.layer.borderWidth=1.0f;
    [_next setImage:[UIImage imageNamed:@"RightArrow"] forState:UIControlStateNormal];
    [self.subView addSubview:_next];
    
    _month = [[UILabel alloc] init];
    _month.textAlignment = NSTextAlignmentCenter;
    [self.subView addSubview:_month];
    
    _monday = [[UILabel alloc] init];
    //_monday.backgroundColor = [UIColor grayColor];
    _monday.textAlignment = NSTextAlignmentCenter;
    _monday.text = @"Пн";
    [self.subView addSubview:_monday];
    
    _tuesday = [[UILabel alloc] init];
   // _tuesday.backgroundColor = [UIColor grayColor];
    _tuesday.textAlignment = NSTextAlignmentCenter;
    _tuesday.text = @"Вт";
    [self.subView addSubview:_tuesday];
    
    _wednesday = [[UILabel alloc] init];
   // _wednesday.backgroundColor = [UIColor grayColor];
    _wednesday.textAlignment = NSTextAlignmentCenter;
    _wednesday.text = @"Ср";
    [self.subView addSubview:_wednesday];
    
    _thursday = [[UILabel alloc] init];
    //_thursday.backgroundColor = [UIColor grayColor];
    _thursday.textAlignment = NSTextAlignmentCenter;
    _thursday.text = @"Чт";
    [self.subView addSubview:_thursday];
    
    _friday = [[UILabel alloc] init];
    //_friday.backgroundColor = [UIColor grayColor];
    _friday.textAlignment = NSTextAlignmentCenter;
    _friday.text = @"Пт";
    [self.subView addSubview:_friday];
    
    _saturday = [[UILabel alloc] init];
   // _saturday.backgroundColor = [UIColor grayColor];
    _saturday.textAlignment = NSTextAlignmentCenter;
    _saturday.text = @"Сб";
    [self.subView addSubview:_saturday];
    
    _sunday = [[UILabel alloc] init];
    //_sunday.backgroundColor = [UIColor grayColor];
    _sunday.textAlignment = NSTextAlignmentCenter;
    _sunday.text = @"Вс";
    [self.subView addSubview:_sunday];
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];

    CGFloat button =self.prev.layer.cornerRadius*2.0f;
    
    self.subView.frame = CGRectMake(0,
                                    0,//self.frame.size.height*0.25f,
                                    self.frame.size.width,
                                    self.frame.size.height);//*0.75f);
    
    CGFloat width = self.subView.bounds.size.width;
    CGFloat height = self.subView.bounds.size.height;
    
    self.prev.frame = CGRectMake(width*0.05f,
                                 height*0.15f,
                                 button,
                                 button);
    
    self.next.frame = CGRectMake(width - self.prev.frame.origin.x - button,
                                 height*0.15f,
                                 button,
                                 button);
    
    self.month.frame = CGRectMake(width/2.0f - width*0.25f/2.0f,
                                  height*0.1f,
                                  width*0.25f,
                                  20.0f);
    CGFloat offset =10.0f;
    CGFloat dayHeight = height*0.05f;
    CGFloat dayY = self.next.frame.origin.y + button + offset;
    CGFloat dayWidth = (width - self.prev.frame.origin.x*2.0f)/7.0f - offset;
    offset *= 1 + 1.0f/6.0f;
    self.monday.frame = CGRectMake(self.prev.frame.origin.x, dayY, dayWidth, dayHeight);
    self.tuesday.frame = CGRectMake( self.monday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
    self.wednesday.frame = CGRectMake(self.tuesday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
    self.thursday.frame = CGRectMake(self.wednesday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
    self.friday.frame = CGRectMake( self.thursday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
    self.saturday.frame = CGRectMake( self.friday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
    self.sunday.frame = CGRectMake( self.saturday.frame.origin.x + dayWidth + offset, dayY, dayWidth, dayHeight);
   
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
